[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Readme | Helpers
================================================

[![Latest Stable Version](https://img.shields.io/packagist/v/finally-a-fast/fafcms-helpers?label=stable&style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-helpers)
[![Latest Version](https://img.shields.io/packagist/v/finally-a-fast/fafcms-helpers?include_prereleases&label=unstable&style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-helpers)
[![PHP Version](https://img.shields.io/packagist/php-v/finally-a-fast/fafcms-helpers/dev-master?style=flat-square)](https://www.php.net/downloads.php)
[![License](https://img.shields.io/packagist/l/finally-a-fast/fafcms-helpers?style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-helpers)
[![Total Downloads](https://img.shields.io/packagist/dt/finally-a-fast/fafcms-helpers?style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-helpers)
[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat-square)](http://www.yiiframework.com/)


This package provides basic helpers for the Finally a fast CMS.

Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run
```
php composer.phar require finally-a-fast/fafcms-helpers
```
or add
```
"finally-a-fast/fafcms-helpers": "dev-master"
```
to the require section of your `composer.json` file.

Documentation
------------

[Documentation](https://www.finally-a-fast.com/) will be added soon at https://www.finally-a-fast.com/.

License
-------

**fafcms-helpers** is released under the MIT License. See the [LICENSE](LICENSE) for details.
