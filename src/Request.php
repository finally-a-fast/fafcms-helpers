<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers;

use Yii;

/**
 * Class Request
 *
 * @package fafcms\helpers
 */
class Request extends \yii\web\Request
{
    /**
     * @var array
     */
    public array $noCsrfValidationRoutes = [];

    public function validateCsrfToken($clientSuppliedToken = null): bool
    {
        if (
            in_array(Yii::$app->getRequest()->getPathInfo(), $this->noCsrfValidationRoutes, true) ||
            strpos(Yii::$app->getRequest()->getPathInfo(), Yii::$app->fafcms->getApiUrl()) === 0
        ) {
            return true;
        }

        return parent::validateCsrfToken($clientSuppliedToken); // TODO: Change the autogenerated stub
    }
}
