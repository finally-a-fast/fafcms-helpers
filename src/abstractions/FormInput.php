<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\abstractions;

use yii\base\BaseObject;
use yii\base\Model;
use fafcms\fafcms\widgets\ActiveForm;

/**
 * Class FormInput
 * @package fafcms\helpers\abstractions
 */
abstract class FormInput extends BaseObject
{
    /**
     * @var ActiveForm
     */
    public $form;

    /**
     * @var Model
     */
    public $model;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $inputName;

    /**
     * @var string
     */
    public $inputId;

    /**
     * @var array
     */
    public $labelOptions = [];

    /**
     * @var array
     */
    public array $fieldOptions = [];

    /**
     * @var string
     */
    public $description;

    /**
     * @var array
     */
    public $options = [];

    /**
     * @var bool
     */
    public bool $linkRelation = false;

    /**
     * @var string
     */
    public static string $columnFormat = 'text';

    /**
     * @param bool $isWidget
     *
     * @return array
     */
    public function getInputOptions(bool $isWidget = false): array
    {
        $options = $this->options;

        if ($isWidget) {
            if (isset($options['name'])) {
                $options['options']['name'] = $options['name'];
            }

            if (isset($options['id'])) {
                $options['options']['id'] = $options['id'];
            }

            if (isset($options['disabled'])) {
                $options['options']['disabled'] = $options['disabled'];
            }

            if (isset($options['readonly'])) {
                $options['options']['readonly'] = $options['readonly'];
            }

            unset($options['id'], $options['name'], $options['disabled'], $options['readonly']);
        }

        return $options;
    }

    /**
     * @return string
     */
    abstract public function run(): string;
}
