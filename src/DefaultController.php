<?php
/**
 * @author    Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license   https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link      https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see       https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since     File available since Release 1.0.0
 */

namespace fafcms\helpers;

use fafcms\fafcms\helpers\StringHelper;
use fafcms\fafcms\items\Html as HtmlItem;
use fafcms\fafcms\models\Projectlanguage;
use Yii;

use yii\base\{InvalidConfigException, Model};

use yii\web\{
    Controller,
    ForbiddenHttpException,
    NotFoundHttpException,
    Response
};

use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\filters\VerbFilter;
use yii\helpers\Html;

use yii\helpers\{
    Inflector,
    Json
};
use yii\rest\CreateAction;
use yii\rest\DeleteAction;
use yii\rest\IndexAction;
use yii\rest\OptionsAction;
use yii\rest\Serializer;
use yii\rest\UpdateAction;
use yii\rest\ViewAction;

/**
 * Class DefaultController
 *
 * @package fafcms\helpers
 */
class DefaultController extends Controller
{
    public static $modelClass;
    public static ?string $updateModelClass = null;
    public static ?string $createModelClass = null;
    public static ?string $indexModelClass = null;
    public static ?string $deleteModelClass = null;

    /**
     * {@inheritdoc}
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (static::$modelClass === null) {
            throw new InvalidConfigException('You must set "public static $modelClass" in controller "' . static::class . '".');
        }

        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => Yii::$app->fafcms->accessRules['default'],
            ],
            //region api
            'contentNegotiator' => [
                'class' => ContentNegotiator::class,
                'only' => ['api-index', 'api-view', 'api-create', 'api-update', 'api-delete', 'api-options'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON
                ],
            ],
            'verbFilter' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'api-index' => ['GET', 'HEAD'],
                    'api-view' => ['GET', 'HEAD'],
                    'api-create' => ['POST'],
                    'api-update' => ['PUT', 'PATCH'],
                    'api-delete' => ['DELETE'],
                ],
            ],
            'authenticator' => [
                'class' => CompositeAuth::class,
            ],
            'rateLimiter' => [
                'class' => RateLimiter::class,
            ],
            //endregion api
        ];
    }

    //region api
    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        return [
            'api-index' => [
                'class' => IndexAction::class,
                'modelClass' => static::$indexModelClass ?? static::$modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'api-view' => [
                'class' => ViewAction::class,
                'modelClass' => static::$updateModelClass ?? static::$modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'api-create' => [
                'class' => CreateAction::class,
                'modelClass' => static::$createModelClass ?? static::$modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                //'scenario' => $this->createScenario,
            ],
            'api-update' => [
                'class' => UpdateAction::class,
                'modelClass' => static::$updateModelClass ?? static::$modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                //'scenario' => $this->updateScenario,
            ],
            'api-delete' => [
                'class' => DeleteAction::class,
                'modelClass' => static::$deleteModelClass ?? static::$modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'api-options' => [
                'class' => OptionsAction::class,
            ],
        ];
    }

    /**
     * Checks the privilege of the current user.
     *
     * This method should be overridden to check whether the current user has the privilege
     * to run the specified action against the specified data model.
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = []): void
    {
        //TODO
    }

    /**
     * {@inheritdoc}
     * @throws InvalidConfigException
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        $apiActions = $this->actions();

        if (isset($apiActions[$action->id])) {
            return $this->serializeData($result);
        }

        return $result;
    }

    /**
     * Serializes the specified data.
     * The default implementation will create a serializer based on the configuration given by [[serializer]].
     * It then uses the serializer to serialize the given data.
     *
     * @param mixed $data the data to be serialized
     *
     * @return mixed the serialized data.
     * @throws InvalidConfigException
     */
    protected function serializeData($data)
    {
        return Yii::createObject(Serializer::class)->serialize($data);
    }
    //endregion api

    public function getIndexButtons($filterModel)
    {
        return [[
            'icon' => 'arrow-left',
            'label' => Yii::t('fafcms-core', 'Back'),
            'url' => Yii::$app->getUser()->getReturnUrl()
        ], [
            'options' => [
                'class' => 'primary'
            ],
            'icon' => 'plus',
            'label' => Yii::t('fafcms-core', 'Create {modelClass}', [
                'modelClass' => $filterModel->getEditData()['singular'],
            ]),
            'url' => ['update']
        ]];
    }

    /**
     * Lists all available models.
     *
     * @return array|string
     * @throws InvalidConfigException
     */
    public function actionIndex()
    {
        $modelClass = static::$indexModelClass ?? static::$modelClass;

        return $this->renderActionContent(Yii::$app->view->renderView('model\\index\\' . $modelClass, null, [
            'modelClass' => $modelClass,
        ]));
    }

    /**
     * Creates a model.
     *
     * @return array|string
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        $modelClass = static::$createModelClass ?? static::$modelClass;

        return $this->renderActionContent(Yii::$app->view->renderView('model\\edit\\' . $modelClass, null, [
            'modelClass' => $modelClass,
        ]));
    }

    /**
     * Updates a model.
     *
     * @param $id
     *
     * @return array|string
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id, $contentViewId = null)
    {
        $modelClass = static::$updateModelClass ?? static::$modelClass;

        return $this->renderActionContent(Yii::$app->view->renderView('model\\edit\\' . $modelClass, $contentViewId, [
            'modelClass' => $modelClass,
            'modelId' => $id
        ]));
    }

    /**
     * @return array|string|Response|null
     * @throws ForbiddenHttpException
     * @throws InvalidConfigException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionTranslate()
    {
        $modelClass = static::$modelClass;
        $ids = Yii::$app->request->get('ids', null);
        $projectLanguageId = Yii::$app->request->get('projectLanguageId', null);

        $url = Yii::$app->getRequest()->getReferrer();

        $authModelClass  = $modelClass::$authModelClass ?? $modelClass;
        $allowed = Yii::$app->user->can('create', ['modelClass' => $authModelClass]);

        if (!$allowed) {
            throw new ForbiddenHttpException(Yii::t('fafcms-core', 'Your not allowed to {action} {model}.', ['action' => Yii::t('fafcms-core', 'create'), 'model' => $modelClass::instance()->getEditDataPlural()]));
        }

        $translations = [];

        if ($ids !== null) {//TODO change to make it possible to handle multiple ids
            $model = $modelClass::find()->where(['id' => $ids])->one();
            $translations = $model->getTranslations()->select('projectlanguage_id')->column();
        }

        if ($projectLanguageId === null) {
            $html = '';
            $languageCount = 0;

            foreach (Yii::$app->fafcms->getCurrentProject()->projectProjectlanguages as $projectLanguage) {
                if (Yii::$app->fafcms->getCurrentProjectLanguageId() === $projectLanguage->id || (in_array($projectLanguage->id, $translations))) {
                    continue;
                }

                $languageCount++;
                $projectLanguageId = $projectLanguage->id;

                $html .= Html::a($projectLanguage->name, [
                    '/' . $modelClass::instance()->getEditDataUrl() . '/translate',
                    'ids' => $ids,
                    'modelClass' => $modelClass,
                    'projectLanguageId' => $projectLanguage->id
                ], ['class' => 'collection-item']);
            }

            if ($languageCount === 0) {
                $modelLabel = $modelClass::instance()->getExtendedLabel(false);

                if (!empty($modelLabel)) {
                    $message = Yii::t('fafcms-core', '{modelClass} "{modelLabel}" has already been translated into all available languages.', [
                        'modelClass' => $modelClass::instance()->getEditData()['singular'],
                        'modelLabel' => $modelLabel
                    ]);
                } else {
                    $message = Yii::t('fafcms-core', '{modelClass} has already been translated into all available languages.', [
                        'modelClass' => $modelClass::instance()->getEditData()['singular'],
                    ]);
                }

                Yii::$app->session->setFlash('warning', $message);
                return $this->redirect($url);
            } elseif ($languageCount > 1) {
                return Yii::$app->view->renderActionContent(Yii::$app->view->renderContentItems([
                    'project-list' => [
                        'class' => HtmlItem::class,
                        'contents' => '<div class="collection">' . $html . '</div>'
                    ],
                ]));
            }
        }

        $projectLanguage = Projectlanguage::find()->where(['id' => $projectLanguageId])->one();

        /**
         * @var $models ActiveRecord[]
         */
        $modelsQuery = $modelClass::find()
            ->where([
                'AND',
                ['projectlanguage_id' => Yii::$app->fafcms->getCurrentProjectLanguageId()],
                [
                    '=',
                    $modelClass::find()
                        ->alias('translations')
                        ->select('COUNT(*)')
                        ->where([
                            'translations.translation_base_id' => new Expression($modelClass::tableName() . '.id'),
                            'translations.projectlanguage_id' => $projectLanguageId
                        ]),
                    0
                ]
            ]);

        if ($ids !== null) {
            $modelsQuery->andWhere(['id' => explode(',', $ids)]);
        }

        $models = $modelsQuery->all();
;
        foreach ($models as $model) {
            $translationResult = $model->translate($projectLanguageId);

            if ($translationResult['success']) {
                $type = 'success';
                $modelLabel = $model->getExtendedLabel(false);

                if (!empty($modelLabel)) {
                    $message = Yii::t('fafcms-core', '{modelClass} "{modelLabel}" has been tranlated to "{language}".', [
                        'modelClass' => $model->getEditData()['singular'],
                        'modelLabel' => $modelLabel,
                        'language' => Projectlanguage::extendedLabel($projectLanguage),
                    ]);
                } else {
                    $message = Yii::t('fafcms-core', '{modelClass} has been tranlated to "{language}".', [
                        'modelClass' => $model->getEditData()['singular'],
                        'language' => Projectlanguage::extendedLabel($projectLanguage),
                    ]);
                }
            } else {
                $type = 'error';
                $modelLabel = $model->getExtendedLabel(false);

                if (!empty($modelLabel)) {
                    $message = Yii::t('fafcms-core', 'Error while saving {modelClass} "{modelLabel}"!', [
                        'modelClass' => $model->getEditData()['singular'],
                        'modelLabel' => $modelLabel
                    ]);
                } else {
                    $message = Yii::t('fafcms-core', 'Error while saving {modelClass}!', [
                        'modelClass' => $model->getEditData()['singular'],
                    ]);
                }

                $message = $message . '<br><br>' . implode('<br><br>', $translationResult['errors']);
            }

            Yii::$app->session->addFlash($type, $message);
        }

        return $this->redirect($url);
    }

    /**
     * @param ActiveRecord $model
     * @param bool         $label
     *
     * @return array
     */
    protected function getAttributes(ActiveRecord $model, bool $label)
    {
        $attributes = $model->getAttributes();

        foreach ($attributes as &$attribute) {
            if (is_array($attribute)) {
                foreach ($attribute as &$attributeData) {
                    if ($attributeData instanceof Model) {
                        $attributeData = $this->getAttributes($attributeData, $label);
                    }
                }

                unset($attributeData);
            }
        }

        unset($attribute);

        if ($label) {
            $result = [];

            $labels = $model->attributeLabels();

            array_walk($attributes, static function($value, $key) use (&$result, $labels) {
                $label = $labels[$key] ?? $key;
                $number = 0;

                while (isset($result[$label . ($number > 0 ? ' ' . $number : '')])) {
                    $number++;
                }

                $result[$label . ($number > 0 ? ' ' . $number : '')] = $value;
            });

            $attributes = $result;
        }

        return $attributes;
    }

    /**
     * @return string|\yii\console\Response|Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \ReflectionException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionExport()
    {
        $modelClass = static::$modelClass;
        $authModelClass = $modelClass::$authModelClass ?? $modelClass;

        $allowed = Yii::$app->user->can('edit', ['modelClass' => $authModelClass]);

        if (!$allowed) {
            throw new ForbiddenHttpException(Yii::t('fafcms-core', 'Your not allowed to {action} {model}.', ['action' => Yii::t('fafcms-core', 'delete'), 'model' => static::$modelClass::instance()->getEditDataPlural()]));
        }

        $ids = Yii::$app->request->get('ids', null);

        if ($ids === null) {
            $models = $modelClass::find()->all();
        } else {
            $models = $modelClass::find()->where(['id' => explode(',', $ids)])->all();
        }

        if ($models === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $format = Yii::$app->getRequest()->get('format', 'json');
        $fileType = 'txt';
        $attributes = [];

        foreach ($models as $model) {
            $attributes[] = $this->getAttributes($model, Yii::$app->getRequest()->get('label', 'false') === 'true');
        }

        if (count($models) === 1) {
            $attributes = $attributes[0];
        }

        if ($format === 'serialize') {
            $result = serialize($attributes);
            Yii::$app->getResponse()->getHeaders()->set('Content-Type',  'text/plain; charset=UTF-8');
        } elseif($format === 'php') {
            $result = '<?php' . PHP_EOL . PHP_EOL . 'return ' . StringHelper::codeExport($attributes) . ';';
            $fileType = 'php';
            Yii::$app->getResponse()->getHeaders()->set('Content-Type',  'text/plain; charset=UTF-8');
        } elseif ($format === 'txt') {
            $result = $this->exportToText($attributes);
            Yii::$app->getResponse()->getHeaders()->set('Content-Type',  'text/plain; charset=UTF-8');
        } else {
            $result = Json::encode($attributes);
            $fileType = 'json';
            Yii::$app->getResponse()->getHeaders()->set('Content-Type',  'application/json; charset=UTF-8');
        }

        $result .= PHP_EOL;

        if (Yii::$app->getRequest()->get('download', 'true') === 'true') {
            if (count($models) === 1) {
                $label = (method_exists($model, 'getEditDataSingular') ? $model->getEditDataSingular() : (new \ReflectionClass($model))->getShortName()) . (method_exists($model, 'getExtendedLabel') ? '-' . $model->getExtendedLabel(false) : '');
            } else {
                $label = (method_exists($model, 'getEditDataPlural') ? $model->getEditDataPlural() : (new \ReflectionClass($model))->getShortName());
            }

            return Yii::$app->response->sendContentAsFile($result, Inflector::slug($label). '.' . $fileType);
        }

        Yii::$app->response->format = Response::FORMAT_RAW;

        return $result;
    }

    /**
     * @param array $attributes
     * @param int   $deep
     *
     * @return string
     */
    private function exportToText(array $attributes, int $deep = 0): string
    {
        array_walk($attributes, function($value, $key) use (&$result, $deep) {
            if (is_array($value)) {
                $value = PHP_EOL . $this->exportToText($value, $deep + 1);
            } else {
                $value .= PHP_EOL;
            }

            $result .= str_repeat(' ', $deep * 4) . $key. ': ' . $value;
        });

        return $result;
    }

    /**
     * Deletes an existing model.
     * If deletion is successful, the browser will be redirected to the previous page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $modelClass = static::$deleteModelClass ?? static::$modelClass;
        $authModelClass = $modelClass::$authModelClass ?? $modelClass;

        $allowed = Yii::$app->user->can('delete', ['modelClass' => $authModelClass, 'modelId' => $id]);

        if (!$allowed) {
            throw new ForbiddenHttpException(Yii::t('fafcms-core', 'Your not allowed to {action} {model}.', ['action' => Yii::t('fafcms-core', 'delete'), 'model' => static::$modelClass::instance()->getEditDataPlural()]));
        }

        $model = $modelClass::findOne($id);

        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($this->deleteModel($model) || $model->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('fafcms-core', '{modelClass} has been deleted!', [
                'modelClass' => $model->getEditData()['singular'],
            ]));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'Error while deleting {modelClass}!', [
                'modelClass' => $model->getEditData()['singular']
            ]).'<br><br>'.implode('<br><br>', $model->getErrorSummary(true)));
        }

        return $this->goBack(Yii::$app->getRequest()->getReferrer());
    }

    /**
     * @param $content
     *
     * @return array|string
     * @throws \Exception
     */
    public function renderActionContent($content)
    {
        return Yii::$app->view->renderActionContent($content);
    }

    /**
     * @param ActiveRecord $model
     *
     * @return bool
     * @throws \Throwable
     */
    protected function deleteModel(ActiveRecord $model): bool
    {
        if ($model->hasAttribute('status')) {
            if ($model->beforeDelete()) {
                $model->status = $model::STATUS_DELETED;
                $model->afterDelete();
                return $model->save(false);
            }
        }

        return false;
    }
}
