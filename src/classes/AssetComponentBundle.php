<?php

declare(strict_types=1);

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\classes;

use fafcms\fafcms\components\ViewComponent;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\AssetBundle;
use Yii;
use yii\helpers\FileHelper;

/**
 * Class AssetComponentBundle
 *
 * @package fafcms\helpers\classes
 */
class AssetComponentBundle extends AssetBundle
{
    public function init()
    {
        $fafcmsFomantic = Yii::getAlias('@runtime/fomantic-ui');

        if (!file_exists($fafcmsFomantic)) {
            FileHelper::copyDirectory(Yii::getAlias('@bower/fomantic-ui/dist'), $fafcmsFomantic);
            FileHelper::copyDirectory(Yii::getAlias('@fafcmsCore/assets/fomantic/config'), $fafcmsFomantic);
        }
    }

    /**
     * @var string|null
     */
    public ?string $componentName = null;

    /**
     * @return string
     */
    protected function getComponentName(): string
    {
        if ($this->componentName !== null) {
            return $this->componentName;
        }

        $componentName = static::class;

        $lastBackslashPosition = strrpos($componentName, '\\');

        if ($lastBackslashPosition !== false) {
            $componentName = substr($componentName, $lastBackslashPosition + 1);
        }

        $assetPosition = strrpos($componentName, 'Asset');

        if ($assetPosition !== false) {
            $componentName = substr($componentName, 0, $assetPosition);
        }

        return $componentName;
    }

    /**
     * @param ViewComponent $view
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function registerAssetFiles($view)
    {
        $manager = $view->getAssetManager();

        foreach ($this->js as $js) {
            if (!is_array($js)) {
                if ($js === null) {
                    continue;
                }

                $js = [$js];
            }

            $file = array_shift($js);
            $options = ArrayHelper::merge($this->jsOptions, $js);

            $component = $this->getComponentName();
            $requiredComponents = [];

            foreach ($this->depends as $depend) {
                $dependInstance = new $depend();

                if ($dependInstance instanceof self && $dependInstance->js !== []) {
                    $requiredComponents[] = $dependInstance->getComponentName();
                }
            }

            if (!isset($options['script'])) {
                $options['onload'] = 'window.faf.components.loaded(\''. $component . '\')';
            }

            if (!isset($options['initScript'])) {
                //$options['initScript'] = /
            }
            $view->registerJs('window.faf.components.register(\''. $component . '\', '. Json::encode($requiredComponents) . ', \'' . $component . '\');', ViewComponent::POS_HEAD);

            $options['fafcmsAsync'] = true;
            $view->registerJsFile($manager->getActualAssetUrl($this, $file), $options);
            //$view->registerWebComponent($manager->getActualAssetUrl($this, $file), $options);
        }

        foreach ($this->css as $css) {
            if (!is_array($css)) {
                if ($css === null) {
                    continue;
                }

                $css = [$css];
            }

            //Yii::$app->view->registerCssFile(Url::to(['theme/asset', 'id' => $component]));
            $file = array_shift($css);
            $options = ArrayHelper::merge($this->cssOptions, $css);
            $view->registerCssFile($manager->getActualAssetUrl($this, $file), $options);
        }
    }
}
