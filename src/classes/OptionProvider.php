<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\classes;

use fafcms\fafcms\models\Projectlanguage;
use fafcms\helpers\traits\OptionTrait;
use yii\base\BaseObject;
use yii\caching\DbDependency;
use yii\helpers\ArrayHelper;
use Closure;
use Yii;

/**
 * Class PluginSetting
 *
 * @package fafcms\helpers\classes
 */
class OptionProvider extends BaseObject
{
    /**
     * @var bool
     */
    public bool $emptyItem = true;

    /**
     * @var string
     */
    public string $emptyItemKey = '';

    /**
     * @var string|Closure
     */
    public $itemKey = 'id';

    /**
     * @var string|Closure
     */
    public $itemLabel = 'name';

    /**
     * @var string|Closure|null
     */
    public $itemLanguageLabel = null;

    /**
     * @var array|string[]
     */
    public array $emptyItemLabel = ['fafcms-core', '- None - '];

    /**
     * @var string|Closure|null
     */
    public $itemGroup = null;

    /**
     * @var array|null
     */
    public ?array $filterByLanguage = null;

    /**
     * @var array|null
     */
    public ?array $select = null;

    /**
     * @var array|null
     */
    public ?array $sort = null;

    /**
     * @var array|null
     */
    public ?array $where = null;

    /**
     * @var array|null
     */
    public ?array $joinWith = null;

    /**
     * @var array|null
     */
    public ?array $orderBy = null;

    /**
     * @var string
     */
    protected string $modelClass;

    /**
     * @return bool
     */
    public function getEmptyItem(): bool
    {
        return $this->emptyItem;
    }

    /**
     * @param bool $emptyItem
     *
     * @return $this
     */
    public function setEmptyItem(bool $emptyItem): self
    {
        $this->emptyItem = $emptyItem;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmptyItemKey(): string
    {
        return $this->emptyItemKey;
    }

    /**
     * @param string $emptyItemKey
     *
     * @return $this
     */
    public function setEmptyItemKey(string $emptyItemKey): self
    {
        $this->emptyItemKey = $emptyItemKey;
        return $this;
    }

    /**
     * @return Closure|string
     */
    public function getItemKey()
    {
        return $this->itemKey;
    }

    /**
     * @param Closure|string $itemKey
     *
     * @return $this
     */
    public function setItemKey($itemKey): self
    {
        $this->itemKey = $itemKey;
        return $this;
    }

    /**
     * @return Closure|string
     */
    public function getItemLabel()
    {
        return $this->itemLabel;
    }

    /**
     * @param Closure|string $itemLabel
     *
     * @return $this
     */
    public function setItemLabel($itemLabel): self
    {
        $this->itemLabel = $itemLabel;
        return $this;
    }

    /**
     * @return Closure|string
     */
    public function getItemLanguageLabel()
    {
        return $this->itemLanguageLabel;
    }

    /**
     * @param Closure|string $itemLanguageLabel
     *
     * @return $this
     */
    public function setItemLanguageLabel($itemLanguageLabel): self
    {
        $this->itemLanguageLabel = $itemLanguageLabel;
        return $this;
    }

    /**
     * @return array|string[]
     */
    public function getEmptyItemLabel(): array
    {
        return $this->emptyItemLabel;
    }

    /**
     * @param array $emptyItemLabel
     *
     * @return $this
     */
    public function setEmptyItemLabel(array $emptyItemLabel): self
    {
        $this->emptyItemLabel = $emptyItemLabel;
        return $this;
    }

    /**
     * @return Closure|string|null
     */
    public function getItemGroup()
    {
        return $this->itemGroup;
    }

    /**
     * @param Closure|string|null $itemGroup
     *
     * @return $this
     */
    public function setItemGroup($itemGroup): self
    {
        $this->itemGroup = $itemGroup;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getFilterByLanguage(): ?array
    {
        return $this->filterByLanguage;
    }

    /**
     * @param array|null $filterByLanguage
     *
     * @return $this
     */
    public function setFilterByLanguage(?array $filterByLanguage): self
    {
        $this->filterByLanguage = $filterByLanguage;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getSelect(): ?array
    {
        return $this->select;
    }

    /**
     * @param array|null $select
     *
     * @return $this
     */
    public function setSelect(?array $select): self
    {
        $this->select = $select;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getSort(): ?array
    {
        return $this->sort;
    }

    /**
     * @param array|null $sort
     *
     * @return $this
     */
    public function setSort(?array $sort): self
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getWhere(): ?array
    {
        return $this->where;
    }

    /**
     * @param array|null $where
     *
     * @return $this
     */
    public function setWhere(?array $where): self
    {
        $this->where = $where;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getJoinWith(): ?array
    {
        return $this->joinWith;
    }

    /**
     * @param array|null $joinWith
     *
     * @return $this
     */
    public function setJoinWith(?array $joinWith): self
    {
        $this->joinWith = $joinWith;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getOrderBy(): ?array
    {
        return $this->orderBy;
    }

    /**
     * @param array|null $orderBy
     *
     * @return $this
     */
    public function setOrderBy(?array $orderBy): self
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    /**
     * @param array|null $properties
     *
     * @return $this
     */
    public function setProperties(?array $properties): self
    {
        foreach ($properties as $name => $value) {
            $this->{'set' . $name}($value);
        }

        return $this;
    }

    /**
     * OptionProvider constructor.
     *
     * @param string     $modelClass
     * @param array|null $config
     */
    public function __construct(string $modelClass, ?array $config = [])
    {
        $this->modelClass = $modelClass;
        $this->setProperties($config);
        parent::__construct();
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        if ($this->select === null) {
            $this->select = [$this->itemKey, $this->itemLabel];
        }

        if ($this->orderBy === null) {
            $this->orderBy = array_fill_keys($this->select, SORT_ASC);
        }

        if ($this->filterByLanguage !== null) {
            $projectlanguages = Yii::$app->dataCache->index(Projectlanguage::class);

            if ($this->itemLanguageLabel === null) {
                $this->itemLanguageLabel = static function($item) use ($projectlanguages) {
                    return '<span class="ui small text right floated">' . Projectlanguage::extendedLabel($projectlanguages[$item['projectlanguage_id']]) . '</span>';
                };
            }

            $this->select[] = $this->modelClass::tableName() . '.projectlanguage_id';

            $itemLabel = $this->itemLabel;
            $itemLanguageLabel = $this->itemLanguageLabel;

            $this->itemLabel = static function($item) use($itemLabel, $itemLanguageLabel) {
                if ($itemLabel instanceof \Closure) {
                    $itemLabel = $itemLabel($item);
                } else {
                    $itemLabel = $item[$itemLabel];
                }

                if ($itemLanguageLabel instanceof \Closure) {
                    $itemLanguageLabel = $itemLanguageLabel($item);
                } else {
                    $itemLanguageLabel = $item[$itemLanguageLabel];
                }

                return $itemLanguageLabel . $itemLabel;
            };

            if ($this->itemLabel instanceof \Closure) {
                $hasClosure = true;
            } else {
                $optionsCacheName .= '_' . $this->itemLabel;
            }
        }

        $options = $this->loadOptions();

        if ($this->emptyItem) {
            $options = [$this->emptyItemKey => Yii::$app->fafcms->getTextOrTranslation($this->emptyItemLabel)] + $options;
        }

        return $options;
    }

    /**
     * @return array
     */
    protected function loadOptions(): array
    {
        $query = $this->modelClass::find()->select($this->select)->orderBy($this->orderBy)->where($this->where)->joinWith($this->joinWith);
        $cacheName = $this->modelClass;

        if (method_exists($this->modelClass, 'getProject_id')) {
            $cacheName .= '_P' . Yii::$app->fafcms->getCurrentProjectId();
        }

        if (method_exists($this->modelClass, 'getProjectlanguage_id')) {
            $cacheName .= '_L' . Yii::$app->fafcms->getCurrentProjectLanguageId();
        }

        $cacheName .= '_' . hash('sha256', $query->createCommand()->getRawSql());

        $optionsCacheName = $cacheName;
        $options = false;
        $hasClosure = false;

        if ($this->itemKey instanceof \Closure) {
            $hasClosure = true;
        } else {
            $optionsCacheName .= '_'.$this->itemKey;
        }

        if ($this->itemLabel instanceof \Closure) {
            $hasClosure = true;
        } else {
            $optionsCacheName .= '_' . $this->itemLabel;
        }

        if ($this->itemGroup instanceof \Closure) {
            $hasClosure = true;
        } else {
            $optionsCacheName .= '_' . $this->itemGroup;
        }

        if (!$hasClosure) {
            $options = Yii::$app->cache->get($optionsCacheName);
        }

        if ($options === false) {
            $dependencyQuery = 'SELECT COUNT(*)' . ($this->modelClass::instance()->hasAttribute('created_at')?', MAX(created_at)':'') . ($this->modelClass::instance()->hasAttribute('updated_at') ? ', MAX(updated_at)' : '') . ($this->modelClass::instance()->hasAttribute('deleted_at') ? ', MAX(deleted_at)' : '').' FROM ' . $this->modelClass::tableName();
            $queryData = Yii::$app->cache->get($cacheName);

            if ($queryData === false) {
                $queryData = $query->asArray();

                if ($this->filterByLanguage !== null) {
                    $queryData = $queryData->byProjectLanguage($this->filterByLanguage);
                }

                $queryData = $queryData->all();

                Yii::$app->cache->set(
                    $cacheName,
                    $queryData,
                    null,
                    new DbDependency(['sql' => $dependencyQuery])
                );
            }

            if ($this->itemKey === 'id' && $this->modelClass::primaryKey()[0] === 'hashId') {
                $this->itemKey = function ($model) {
                    return Yii::$app->fafcms->idToHash($model['id'], $this->modelClass);
                };
            }

            $options = ArrayHelper::map($queryData, $this->itemKey, $this->itemLabel, $this->itemGroup);

            if (!$hasClosure) {
                Yii::$app->cache->set(
                    $optionsCacheName,
                    $options,
                    null,
                    new DbDependency(['sql' => $dependencyQuery])
                );
            }
        }

        return $options;
    }
}
