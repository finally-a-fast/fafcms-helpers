<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\migrations;

use fafcms\fafcms\models\Project;
use fafcms\helpers\ActiveRecord;
use fafcms\sitemanager\models\Site;
use fafcms\updater\base\Migration;
use Yii;

/**
 * Class MultiProjectMigration
 *
 * @package fafcms\helpers\migrations
 */
class MultiProjectMigration extends Migration
{
    public string $modelClass;

    public function safeUp(): bool
    {
        $cleanTableName = str_replace(['{{%', '}}'], '', $this->modelClass::prefixableTableName());

        $this->addColumn($this->modelClass::tableName(), 'project_id', $this->integer(10)->unsigned()->null()->defaultValue(null)->after('id'));

        $this->modelClass::updateAll(['project_id' => Yii::$app->fafcms->getCurrentProjectId()]);

        $this->createIndex('idx-' . $cleanTableName . '-project_id', $this->modelClass::tableName(), ['project_id'], false);
        $this->addForeignKey('fk-' . $cleanTableName . '-project_id', $this->modelClass::tableName(), 'project_id', Project::tableName(), 'id', 'CASCADE', 'CASCADE');

        return true;
    }

    public function safeDown(): bool
    {
        $cleanTableName = str_replace(['{{%', '}}'], '', $this->modelClass::prefixableTableName());

        $this->dropForeignKey('fk-' . $cleanTableName . '-project_id', $this->modelClass::tableName());
        $this->dropIndex('idx-' . $cleanTableName . '-project_id', $this->modelClass::tableName());
        $this->dropColumn($this->modelClass::tableName(), 'project_id');

        return true;
    }
}
