<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\migrations;

use fafcms\fafcms\models\Projectlanguage;
use fafcms\helpers\ActiveRecord;
use fafcms\sitemanager\models\Site;
use fafcms\updater\base\Migration;
use Yii;

/**
 * Class MultilingualMigration
 *
 * @package fafcms\helpers\migrations
 */
class MultilingualMigration extends Migration
{
    public string $modelClass;

    public function safeUp(): bool
    {
        $cleanTableName = str_replace(['{{%', '}}'], '', $this->modelClass::prefixableTableName());

        $this->addColumn($this->modelClass::tableName(), 'translation_base_id', $this->integer(10)->unsigned()->null()->defaultValue(null)->after('id'));
        $this->addColumn($this->modelClass::tableName(), 'projectlanguage_id', $this->integer(10)->unsigned()->null()->defaultValue(null)->after('translation_base_id'));

        $this->modelClass::updateAll(['projectlanguage_id' => Yii::$app->fafcms->getCurrentProjectLanguageId()]);

        $this->createIndex('idx-' . $cleanTableName . '-translation_base_id', $this->modelClass::tableName(), ['translation_base_id'], false);
        $this->createIndex('idx-' . $cleanTableName . '-projectlanguage_id', $this->modelClass::tableName(), ['projectlanguage_id'], false);

        $this->addForeignKey('fk-' . $cleanTableName . '-translation_base_id', $this->modelClass::tableName(), 'translation_base_id', $this->modelClass::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-' . $cleanTableName . '-projectlanguage_id', $this->modelClass::tableName(), 'projectlanguage_id', Projectlanguage::tableName(), 'id', 'CASCADE', 'CASCADE');

        return true;
    }

    public function safeDown(): bool
    {
        $cleanTableName = str_replace(['{{%', '}}'], '', $this->modelClass::prefixableTableName());

        $this->dropForeignKey('fk-' . $cleanTableName . '-translation_base_id', $this->modelClass::tableName());
        $this->dropForeignKey('fk-' . $cleanTableName . '-projectlanguage_id', $this->modelClass::tableName());

        $this->dropIndex('idx-' . $cleanTableName . '-translation_base_id', $this->modelClass::tableName());
        $this->dropIndex('idx-' . $cleanTableName . '-projectlanguage_id', $this->modelClass::tableName());

        $this->dropColumn($this->modelClass::tableName(), 'translation_base_id');
        $this->dropColumn($this->modelClass::tableName(), 'projectlanguage_id');

        return true;
    }
}
