<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers;

use fafcms\fafcms\{components\InjectorComponent, helpers\StringHelper, models\Projectlanguage, queries\DefaultQuery};
use fafcms\helpers\{behaviors\AttributesBehavior, traits\HashIdTrait, traits\ModelTrait, traits\MultilingualTrait};
use Yii;
use yii\{base\InvalidArgumentException, base\InvalidCallException, base\InvalidConfigException, db\ActiveQuery, db\ActiveQueryInterface, helpers\ArrayHelper, helpers\Inflector};

/**
 * Class ActiveRecord
 *
 * @package fafcms\helpers
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    use HashIdTrait;
    use ModelTrait;

    public const EVENT_BEFORE_ACTIVATE   = 'beforeActivate';
    public const EVENT_BEFORE_DEACTIVATE = 'beforeDeactivate';

    public const SCENARIO_CREATE = 'create';
    public const SCENARIO_UPDATE = 'update';
    public const SCENARIO_DELETE = 'delete';
    public const SCENARIO_SEARCH = 'search';

    public const STATUS_ACTIVE   = 'active';
    public const STATUS_INACTIVE = 'inactive';
    public const STATUS_DELETED  = 'deleted';

    public const RESULT_ACTION_MERGE = 'merge';
    public const RESULT_ACTION_BOOL  = 'bool';

    public bool $fafcmsLogChange = true;

    private array $_scenarios = [];

    public static string $tablePrefix = '';

    /**
     * @param int $projectLanguageId
     *
     * @return bool[]
     * @throws InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function translate(int $projectLanguageId): array
    {
        $modelClass = static::class;
        $model = $this;
        $result = [
            'errors' => [],
            'success' => true,
        ];

        /**
         * @var $modelTranslation ActiveRecord
         */
        $modelTranslation = InjectorComponent::createObject($modelClass);
        $modelScenarios = $modelTranslation->scenarios();

        if (isset($scenario, $modelScenarios[$scenario]) || (defined($modelClass . '::SCENARIO_CREATE') && isset($modelScenarios[$modelClass::SCENARIO_CREATE]))) {
            $modelTranslation->setScenario($scenario ?? $modelClass::SCENARIO_CREATE);
        }

        $modelTranslation->loadDefaultValues();

        $attributeNames = [];

        foreach ($modelTranslation->getFieldConfig() as $fieldName => $fieldConfig) {
            if (in_array($fieldName, [
                'id',
                'created_by',
                'updated_by',
                'activated_by',
                'deactivated_by',
                'deleted_by',
                'created_at',
                'updated_at',
                'activated_at',
                'deactivated_at',
                'deleted_at',
                'topicids'//todo
            ])) {
                continue;
            }

            $translatedValue = $model->getAttribute($fieldName);

            if (isset($fieldConfig['relationClassName']) &&
                $translatedValue !== null &&
                method_exists($fieldConfig['relationClassName'], 'getProjectlanguage_id') &&
                (
                    $newModel = $fieldConfig['relationClassName']::find()->where([
                        'translation_base_id' => $translatedValue
                    ])->select('id')->byProjectLanguage($projectLanguageId)->asArray()->one()
                ) !== null
            ) {
                $translatedValue = (int)$newModel['id'];
            } else {
                $attributeNames[] = $fieldName;
            }

            try {
                // todo multiple relations like article files
                if ($fieldName === 'status') {
                    $translatedValue = 'inactive';
                }

                $modelTranslation->setAttribute($fieldName, $translatedValue);
            } catch (InvalidArgumentException $e) {}
        }

        $modelTranslation->translation_base_id = $model->id;
        $modelTranslation->projectlanguage_id = $projectLanguageId;

        $modelRelations = Yii::$app->fileCache->getOrSet('model_relations_' . $modelClass, static function() use($model) {
            $modelRelations = [];
            $reflection = new \ReflectionClass($model);
            $modelMethods = $reflection->getMethods(\ReflectionMethod::IS_PUBLIC);

            foreach ($modelMethods as $modelMethod) {
                try {
                    if (stripos($modelMethod->name, 'get') === 0 &&
                        !$modelMethod->isStatic() &&
                        $modelMethod->getNumberOfRequiredParameters() === 0
                    ) {
                        $relation = $model->{$modelMethod->name}();

                        if ($relation instanceof ActiveQueryInterface) {
                            $relationName = lcfirst(substr($modelMethod->name, 3));
                            $translateable = false;

                            if (in_array($relationName, array_merge([
                                'createdBy',
                                'updatedBy',
                                'activatedBy',
                                'deactivatedBy',
                                'deletedBy',
                                'projectlanguage',
                                'translations',
                                'contentmetas' // remove when relation has been deleted
                            ], $model->getTranslateIgnoreRelations()))) {
                                continue;
                            }

                            $relationReflection = new \ReflectionClass($relation->modelClass);

                            if (in_array(MultilingualTrait::class, $relationReflection->getTraitNames())) {
                                $translateable = true;
                            }

                            $modelRelations[] = [
                                'relationName' => $relationName,
                                'methodName' => $modelMethod->name,
                                'model' => $relation->modelClass,
                                'translateable' => $translateable,
                                'autoTranslate' => $relation->modelClass::$autoTranslate ?? false
                            ];
                        }
                    }
                } catch (\Exception $e) {}
            }

            return $modelRelations;
        }, 120);

        $projectLanguage = Projectlanguage::find()->where(['id' => $projectLanguageId])->one();

        $transaction = Yii::$app->db->beginTransaction();

        if ($modelTranslation->validate($attributeNames) && $modelTranslation->save(false)) {
            foreach ($modelRelations as $modelRelation) {
                $relationQuery = $model->{$modelRelation['methodName']}();
                $relations = ArrayHelper::index($relationQuery
                    ->byProjectLanguage([$model->projectlanguage_id, $projectLanguageId])
                    ->all(),
                    static function($model) {
                        return ($model->translation_base_id ?? $model->id) . '_' . ($model->projectlanguage_id ?? '');
                    }
                );

                    /*echo '<hr>';
                    var_dump($modelRelation['relationName']);
                    var_dump(count($relations));

                var_dump($relationQuery->multiple);
                var_dump($relationQuery->modelClass);
                    if ($modelRelation['relationName'] === 'parentContentmetaContentmetas') {
                       // var_dump($relationQuery);
                        foreach ($relations as $ret) {
                            var_dump($ret->getAttributes());
                        }
                    }*/

                if ($relations !== []) {
                    $filteredRelations = $relations;

                    foreach ($relations as $relation) {
                        if (($relation->projectlanguage_id ?? null) !== null && $relation->projectlanguage_id === $projectLanguageId) {
                            unset($filteredRelations[$relation->translation_base_id . '_' . $model->projectlanguage_id]);
                        }
                    }

                    foreach ($filteredRelations as $filteredRelation) {
                        //todo change tag relation
                        if ($modelRelation['relationName'] === 'tagModels') {
                            $modelTranslation->setTags($model->getTags());

                            if (!$modelTranslation->save()) {
                                $result['errors'] = array_merge($result['errors'], $modelTranslation->getErrorSummary(true));
                                $result['success'] = false;
                                break 2;
                            }
                        } else {
                            if ($modelRelation['translateable']) {
                                if ($filteredRelation->projectlanguage_id !== $projectLanguageId) {
                                    $translatedRelation = $filteredRelation
                                        ->getTranslations()
                                        ->where(['projectlanguage_id' => $projectLanguageId])
                                        ->one();

                                    if ($translatedRelation === null) {
                                        if ($modelRelation['autoTranslate'] || in_array($modelRelation['relationName'], $model->getAutoTranslateRelations())) {
                                            $translationResult = $filteredRelation->translate($projectLanguageId);

                                            if ($translationResult['success']) {
                                                $translatedRelation = $filteredRelation
                                                    ->getTranslations()
                                                    ->where(['projectlanguage_id' => $projectLanguageId])
                                                    ->one();
                                            } else {
                                                $result['errors'] = array_merge($result['errors'], $translationResult['errors']);
                                                $result['success'] = false;
                                                break 2;
                                            }

                                            $filteredRelation = $translatedRelation;
                                        }
                                    } else {
                                        $filteredRelation = $translatedRelation;
                                    }
                                }
                            }

                            if (!$relationQuery->multiple || ($modelRelation['translateable'] && $filteredRelation->projectlanguage_id === $projectLanguageId)) {
                                /**
                                 * @var $filteredRelation ActiveRecord
                                 */
                                try {
                                    $modelTranslation->link($modelRelation['relationName'], $filteredRelation);
                                } catch (InvalidCallException $e) {
                                    $result['errors'][] = $e->getMessage();
                                    $result['success'] = false;
                                    break 2;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $result['errors'] = array_merge($result['errors'], $modelTranslation->getErrorSummary(true));
            $result['success'] = false;
        }

        if ($result['success']) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
        }

        return $result;
    }

    /**
     * Declares the name of the database table associated with this AR class.
     * By default this method returns the class name as the table name by calling [[Inflector::camel2id()]].
     * @return string the table name
     */
    public static function prefixableTableName(): string
    {
        $className = InjectorComponent::getClass(static::class);
        return '{{%' . Inflector::camel2id(StringHelper::basename($className), '_') . '}}';
    }

    /**
     * @return object|string|ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public static function find()
    {
        $modelClass = InjectorComponent::getClass(static::class);
        $queryClass = InjectorComponent::getActiveQueryForModel($modelClass) ?? InjectorComponent::getActiveQueryForModel(static::class) ?? InjectorComponent::getClass(DefaultQuery::class);

        return InjectorComponent::createObject($queryClass, [$modelClass]);
    }

    /**
     * Warning - You should should not overwrite this. Please only overwrite [[ActiveRecord::prefixableTableName()]]!
     * {@inheritdoc}
     */
    public static function tableName()
    {
        $className = InjectorComponent::getClass(static::class);
        $classParts = explode('\\', $className);
        $lastContentmeta = null;
        $tablePrefix = $className::$tablePrefix;

        if ($tablePrefix === '') {
            $modelTablePrefixes = Yii::$app->fafcms->modelTablePrefixes ?? [
                'fafcms\\fafcms' => 'fafcms-core_' // core module probably isn't loaded at this moment
            ];

            do {
                $classTarget = implode('\\', $classParts);

                if (isset($modelTablePrefixes[$classTarget])) {
                    $tablePrefix = $modelTablePrefixes[$classTarget];
                    break;
                }

                if (isset($modelTablePrefixes[$classTarget . '\\'])) {
                    $tablePrefix = $modelTablePrefixes[$classTarget . '\\'];
                    break;
                }

                array_pop($classParts);
            } while (count($classParts) > 0);
        }

        return str_replace('%', '%' . $tablePrefix, $className::prefixableTableName());
    }

    /**
     * {@inheritDoc}
     */
    public function beforeSave($insert): bool
    {
        if ($this->hasAttribute('status') && $this->isAttributeChanged('status')) {
            if ($this->status === static::STATUS_ACTIVE) {
                $this->beforeActivate();
            } elseif ($this->status === static::STATUS_INACTIVE) {
                $this->beforeDeactivate();
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return void
     */
    public function beforeActivate(): void
    {
        $this->trigger(static::EVENT_BEFORE_ACTIVATE);
    }

    /**
     * @return void
     */
    public function beforeDeactivate(): void
    {
        $this->trigger(static::EVENT_BEFORE_DEACTIVATE);
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'default' => [
                'class' => AttributesBehavior::class,
                'owner' => $this,
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        if (!$this->_scenarios) {
            $scenarios                          = parent::scenarios();
            $scenarios[static::SCENARIO_CREATE] = $scenarios['default'];
            $scenarios[static::SCENARIO_UPDATE] = $scenarios['default'];
            $scenarios[static::SCENARIO_DELETE] = $scenarios['default'];
            $scenarios[self::SCENARIO_SEARCH]   = $scenarios['default'];
            $this->_scenarios                   = $scenarios;
        }

        return $this->_scenarios;
    }
}
