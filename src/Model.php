<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers;

use fafcms\fafcms\components\InjectorComponent;
use fafcms\fafcms\queries\ArrayQuery;
use fafcms\helpers\traits\ModelTrait;
use yii\base\InvalidConfigException;
use yii\db\ExpressionInterface;
use yii\helpers\ArrayHelper;

/**
 * Class Model
 *
 * @package fafcms\helpers
 */
class Model extends \yii\base\Model
{
    use ModelTrait;

    public const RESULT_ACTION_MERGE = 'merge';
    public const RESULT_ACTION_BOOL  = 'bool';

    public const EVENT_AFTER_FIND = 'afterFind';

    public const SCENARIO_SEARCH = 'search';

    /**
     * @return string[]
     */
    public static function primaryKey(): array
    {
        return ['id'];
    }

    /**
     * @return array
     */
    public static function getRecords(): array
    {
        return [];
    }

    public static function populateRecord($record, $row)
    {
        $columns = array_flip($record->attributes());

        foreach ($row as $name => $value) {
            if (isset($columns[$name])) {
                $record->$name = $value;
            }
        }
    }

    /**
     * @param $condition
     *
     * @return mixed
     * @throws InvalidConfigException
     */
    public static function findOne($condition)
    {
        return static::findByCondition($condition)->one();
    }

    protected static function findByCondition($condition)
    {
        $query = static::find();

        if (!ArrayHelper::isAssociative($condition) && !$condition instanceof ExpressionInterface) {
            // query by primary key
            $primaryKey = static::primaryKey();

            if (isset($primaryKey[0])) {
                // if condition is scalar, search for a single primary key, if it is array, search for multiple primary key values
                $condition = [$primaryKey[0] => is_array($condition) ? array_values($condition) : $condition];
            } else {
                throw new InvalidConfigException('"' . get_called_class() . '" must have a primary key.');
            }
        }

        return $query->andWhere($condition);
    }

    /**
     * @return object
     * @throws \yii\base\InvalidConfigException
     */
    public static function find(): object
    {
        $modelClass = InjectorComponent::getClass(static::class);
        $queryClass = InjectorComponent::getActiveQueryForModel($modelClass) ?? InjectorComponent::getActiveQueryForModel(static::class) ?? InjectorComponent::getClass(ArrayQuery::class);

        return InjectorComponent::createObject($queryClass, [$modelClass]);
    }

    public function afterFind()
    {
        $this->trigger(self::EVENT_AFTER_FIND);
    }
}
