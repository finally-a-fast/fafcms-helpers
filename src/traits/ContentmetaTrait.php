<?php

namespace fafcms\helpers\traits;

/**
 * Trait ContentmetaTrait
 *
 * @package fafcms\helpers\traits
 * @deprecated use \fafcms\sitemanager\traits\ContentmetaTrait instead
 */
trait ContentmetaTrait
{
    use \fafcms\sitemanager\traits\ContentmetaTrait;
}
