<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\traits;

use fafcms\fafcms\inputs\ExtendedDropDownList;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\models\Project;
use fafcms\helpers\migrations\MultiProjectMigration;
use Yii;
use yii\db\ActiveQuery;
use Closure;

/**
 * Trait MultiProjectTrait
 *
 * @package fafcms\helpers\traits
 */
trait MultiProjectTrait
{
    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getDefaultEditViewItemsMultiprojectTrait(): array
    {
        return [
            'tab-1' => [
                'contents' => [
                    'row-1' => [
                        'contents' => [
                            'column-2' => [
                                'contents' => [
                                    'project-card' => [
                                        'class' => \fafcms\fafcms\items\Card::class,
                                        'settings' => [
                                            'title' => Yii::t('fafcms-core', 'Project assignment'),
                                            'icon' => 'scatter-plot-outline',
                                        ],
                                        'contents' => [
                                            'field-project_id' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'project_id',
                                                ],
                                            ],
                                        ]
                                    ],
                                ],
                            ],
                        ],
                    ],
                ]
            ],
        ];
    }

    /**
     * @return int|null
     */
    public function getProject_id(): ?int
    {
        try {
            return $this->project_id;
        } catch (\ErrorException $e) {
            if ($e->getCode() === 8) {
                $migration = new MultiProjectMigration([
                    'modelClass' => self::class,
                    'compact' => true
                ]);

                if ($migration->up()) {
                    return $this->project_id;
                }
            }
        }

        return null;
    }

    /**
     * @return array[]
     */
    public function getFieldConfigMultiprojectTrait(): array
    {
        return [
            'project_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('project_id', false),
                'relationClassName' => Project::class,
            ],
        ];
    }

    /**
     * @return Closure[]
     */
    public function attributeOptionsMultiprojectTrait(): array
    {
        return [
            'project_id' => static function($properties = []) {
                return Project::getOptionProvider($properties)->getOptions();
            },
        ];
    }

    /**
     * @param bool $skipIfSet
     *
     * @return $this
     * @throws \Throwable
     */
    public function fafcmsLoadDefaultValuesMultiprojectTrait($skipIfSet = true): self
    {
        if (!$skipIfSet || $this->project_id === null) {
            $this->project_id = Yii::$app->fafcms->getCurrentProjectId();
        }

        return $this;
    }

    public function fafcmsRulesMultiprojectTrait(): array
    {
        return [
            'integer-project_id' => ['project_id', 'integer'],
        ];
    }

    /**
     * @return Closure
     */
    /* TODO COPY to project
     * public function getEditViewButtonsMultiprojectTrait(): Closure
    {
        return static function($buttons, $model, $form, $editView) {
            if (!$model->isNewRecord) {
                $buttons[] = [
                    'icon' => 'translate',
                    'label' => Yii::t('fafcms-core', 'Translate'),
                    'url' => $model->getAbsoluteUrl(),
                ];
            }

            return $buttons;
        };
    }*/

    /**
     * Gets query for [[Project]].
     *
     * @return ActiveQuery
     */
    public function getProject(): ActiveQuery
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }
}
