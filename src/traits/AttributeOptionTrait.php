<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\traits;

use fafcms\helpers\ActiveRecord;
use fafcms\helpers\Model;

/**
 * Trait AttributeOptionTrait
 *
 * @package fafcms\helpers\traits
 */
trait AttributeOptionTrait
{
    /**
     * @return \Closure[]|array[]
     */
    abstract public function attributeOptions(): array;

    /**
     * @param string|null $attributeName
     * @param bool|null   $callClosure
     * @param mixed       ...$params
     *
     * @return \Closure[]|array[]
     */
    public function getAttributeOptions(?string $attributeName = null, ?bool $callClosure = true, ...$params)
    {

        $method_names = preg_grep('/^attributeOptions/', get_class_methods($this));
        $fieldConfig = [];

        foreach($method_names as $method) {
            $fieldConfig[] = $this->$method();
        };

        $attributeOptions = array_merge(...$fieldConfig);

        if ($attributeName !== null) {
            $attributeOptions = $attributeOptions[$attributeName] ?? [];
        }

        if ($callClosure && $attributeOptions instanceof \Closure) {
            return $attributeOptions(...$params);
        }

        return $attributeOptions;
    }
}
