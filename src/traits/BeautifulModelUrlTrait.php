<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\traits;

use fafcms\helpers\ActiveRecord;
use fafcms\helpers\Model;

/**
 * Trait BeautifulModelUrlTrait
 *
 * @package fafcms\helpers\traits
 */
trait BeautifulModelUrlTrait
{
    /**
     * @param ActiveRecord|Model $model
     *
     * @return string
     */
    abstract public static function editDataUrl($model): string;

    /**
     * @return string
     */
    public function getEditDataUrl(): string
    {
        return static::editDataUrl($this);
    }
}
