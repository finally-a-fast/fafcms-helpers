<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\traits;

use fafcms\helpers\classes\OptionProvider;
use Yii;

/**
 * Trait OptionProviderTrait
 *
 * @package fafcms\helpers\traits
 */
trait OptionProviderTrait
{
    /**
     * @param array $properties
     *
     * @return OptionProvider
     */
    abstract public static function getOptionProvider(array $properties = []): OptionProvider;
}
