<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\traits;

use fafcms\fafcms\inputs\Chips;
use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\fafcms\models\Tag;
use fafcms\fafcms\models\Tagrealation;
use Yii;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\db\ActiveQueryInterface;
use yii\helpers\ArrayHelper;

/**
 * Trait TagTrait
 * @package fafcms\helpers\traits
 */
trait TagTrait
{
    /**
     * @return array
     */
    public function getFieldConfigTagTrait(): array
    {
        return [
            'tags' => [
                'type' => Chips::class,
                'items' => static function($properties = []) {
                    $properties['select'] = [
                        Tag::tableName() . '.name'
                    ];

                    $properties['itemKey'] = 'name';

                    return Tag::getOptionProvider($properties)->getOptions();
                }
            ],
        ];
    }

    /**
     * @return array
     */
    public function getDefaultEditViewItemsTagTrait(): array
    {
        return [
            'categorization-tab' => [
                'class' => Tab::class,
                'settings' => [
                    'label' => ['category' => 'fafcms-core', 'message' => 'Categorization'],
                ],
                'contents' => [
                    'row-tags' => [
                        'class' => Row::class,
                        'contents' => [
                            'column-1' => [
                                'class' => Column::class,
                                'contents' => [
                                    'card-1' => [
                                        'class' => Card::class,
                                        'settings' => [
                                            'title' => Tag::instance()->getEditDataPlural(),
                                            'icon' => Tag::instance()->getEditDataIcon(),
                                        ],
                                        'contents' => [
                                            [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'tags',
                                                ],
                                            ],
                                        ]
                                    ],
                                ]
                            ],
                        ],
                    ],
                ]
            ]
        ];
    }

    /**
     * @var string
     */
    private $_tags;

    /**
     * @return array
     */
    public function getTags(): array
    {
        if ($this->_tags === null) {
            $this->_tags = [];

            if ($this->tagModels !== null) {
                $this->_tags = ArrayHelper::getColumn($this->tagModels, 'name');
            }
        }

        return $this->_tags;
    }

    /**
     * @return array
     */
    public function setTags($value)
    {
        if (is_string($value)) {
            if ($value === '') {
                $value = null;
            } else {
                $value = explode(',', $value);
            }
        }

        $this->_tags = $value;
    }

    /**
     * @return ActiveQueryInterface|null
     */
    public function getTagModels(): ?ActiveQueryInterface
    {
        if (method_exists($this, 'hasMany')) {
            return $this->hasMany(Tag::class, ['id' => 'tag_id'])->viaTable(Tagrealation::tableName(), ['model_id' => 'id'], static function (ActiveQuery $query) {
                $query->andWhere(['model_class' => self::class]);
            });
        }

        return null;
    }

    /**
     * @param $runValidation
     * @param $attributeNames
     * @return bool
     */
    public function fafcmsSaveTagTrait($runValidation, $attributeNames): bool
    {
        $savedTags = ArrayHelper::getColumn($this->tagModels, 'name');

        $deletedTagIds = Tag::find()->where(['name' => array_diff($savedTags, $this->tags)])->select('id')->column();

        $newTags = array_diff($this->tags, $savedTags);
        $tagIds = ArrayHelper::map(Tag::find()->where(['name' => $newTags])->select('id, LOWER(name) AS name')->asArray()->all(), 'name', 'id');

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $deletedCount = Tagrealation::deleteAll([
                'model_class' => self::class,
                'model_id' => $this->id,
                'tag_id' => $deletedTagIds,
            ]);

            if ($deletedCount < count($deletedTagIds)) {
                throw new Exception('Not all tags could be deleted.');
            }

            foreach ($newTags as $newTag) {
                $tagIndex = mb_strtolower($newTag);

                if (!isset($tagIds[$tagIndex])) {
                    $tagModel = new Tag();
                    $tagModel->loadDefaultValues();

                    $tagModelData = [
                        $tagModel->formName() => [
                            'name' => $newTag,
                            'relations' => [
                                'contentmeta' => [
                                    'NEW' => [
                                        'type' => 'content',
                                        'title' => $newTag
                                    ]
                                ]
                            ]
                        ]
                    ];

                    $modelResult = Yii::$app->fafcms->saveModelData($tagModel, $tagModelData);

                    if ($modelResult !== 2 && $modelResult !== 3) {
                        throw new Exception('Could not save new tag "' . $newTag . '". ' . print_r($tag->getErrorSummary(true), true));
                    }

                    $tagIds[$tagIndex] = $tagModel->id;
                }

                $tagrealation = new Tagrealation([
                    'model_class' => self::class,
                    'model_id' => $this->id,
                    'tag_id' => $tagIds[$tagIndex],
                ]);
                $tagrealation->loadDefaultValues();

                if (!$tagrealation->save()) {
                    throw new Exception('Could not save new tag relation for "' . $newTag . '". ' . print_r($tagrealation->getErrorSummary(true), true));
                }
            }

            $transaction->commit();
            return true;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $this->addError('tags', $e->getMessage());
            return false;
        }
    }

    public function fafcmsRulesTagTrait(): array
    {
        return [['tags', 'safe']];
    }

    public function fafcmsAttributeLabelsTagTrait(): array
    {
        return [
            'tags' => Yii::t('fafcms-core', 'Tags'),
        ];
    }
}
