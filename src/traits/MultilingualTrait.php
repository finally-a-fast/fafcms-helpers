<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\traits;

use fafcms\fafcms\inputs\ExtendedDropDownList;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\fafcms\models\Projectlanguage;
use fafcms\helpers\migrations\MultilingualMigration;
use Yii;
use yii\db\ActiveQuery;
use Closure;

/**
 * Trait MultilingualTrait
 *
 * @package fafcms\helpers\traits
 */
trait MultilingualTrait
{
    use MultiProjectTrait;

    /**
     * @return string[]
     */
    public function getTranslateIgnoreRelations(): array
    {
        $result = [];
        $this->executeExtendedMethods('translateIgnoreRelations', [], $result, self::RESULT_ACTION_MERGE);
        return $result;
    }

    /**
     * @return string[]
     */
    public function getAutoTranslateRelations(): array
    {
        $result = [];
        $this->executeExtendedMethods('autoTranslateRelations', [], $result, self::RESULT_ACTION_MERGE);
        return $result;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getDefaultEditViewItemsMultilingualTrait(): array
    {
        return [
            'tab-1' => [
                'class' => Tab::class,
                'settings' => [
                    'label' => [
                        'fafcms-core',
                        'Master data',
                    ],
                ],
                'contents' => [
                    'row-1' => [
                        'class' => Row::class,
                        'contents' => [
                            'column-2' => [
                                'class' => Column::class,
                                'settings' => [
                                    'm' => 5,
                                ],
                                'contents' => [
                                    'project-card' => [
                                        'class' => \fafcms\fafcms\items\Card::class,
                                        'settings' => [
                                            'title' => Yii::t('fafcms-core', 'Project assignment'),
                                            'icon' => 'scatter-plot-outline',
                                        ],
                                        'contents' => [
                                            'field-projectlanguage_id' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'projectlanguage_id',
                                                ],
                                            ],
                                            'field-translation_base_id' => [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'translation_base_id',
                                                ],
                                            ],
                                        ]
                                    ],
                                ],
                            ],
                        ],
                    ],
                ]
            ],
        ];
    }

    /**
     * @param string $column
     *
     * @return int|null
     */
    protected function getOrCreateColumn(string $column): ?int
    {
        try {
            return ($this->$column === '' ? null : $this->$column);
        } catch (\ErrorException $e) {
            if ($e->getCode() === 8) {
                $migration = new MultilingualMigration([
                    'modelClass' => self::class,
                    'compact' => true
                ]);

                if ($migration->up()) {
                    return ($this->$column === '' ? null : $this->$column);
                }
            }
        }

        return null;
    }

    /**
     * @return int|null
     */
    public function getTranslation_base_id(): ?int
    {
        return $this->getOrCreateColumn('translation_base_id');
    }

    /**
     * @return int|null
     */
    public function getProjectlanguage_id(): ?int
    {
        return $this->getOrCreateColumn('projectlanguage_id');
    }

    /**
     * @return array[]
     */
    public function getFieldConfigMultilingualTrait(): array
    {
        return [
            'translation_base_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('translation_base_id', false),
                'relationClassName' => self::class,
            ],
            'projectlanguage_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('projectlanguage_id', false),
                'relationClassName' => Projectlanguage::class,
            ],
        ];
    }

    /**
     * @return Closure[]
     */
    public function attributeOptionsMultilingualTrait(): array
    {
        return [
            'translation_base_id' => function($properties = []) {
                return self::getOptionProvider($properties)
                    ->setFilterByLanguage(['<>', 'projectlanguage_id', $this->projectlanguage_id])
                    ->getOptions();
            },
            'projectlanguage_id' => static function($properties = []) {
                return Projectlanguage::getOptionProvider($properties)->getOptions();
            },
        ];
    }

    /**
     * @return Closure
     */
    public function getEditViewButtonsMultilingualTrait(): Closure
    {
        return static function($buttons, $model, $form, $editView) {
            if (!$model->isNewRecord && Yii::$app->fafcms->getCurrentProjectLanguageId() === Yii::$app->fafcms->getCurrentProject()->primary_projectlanguage_id) {
                $buttons['more']['items']['translate'] = [
                    'icon' => 'translate',
                    'label' => Yii::t('fafcms-core', 'Translate'),
                    'url' => [
                        '/' . $model->getEditDataUrl() . '/translate',
                        'ids' => $model->id,
                    ],
                ];
            }

            return $buttons;
        };
    }

    public function fafcmsRulesMultilingualTrait(): array
    {
        return [
            'integer-translation_base_id' => ['translation_base_id', 'integer'],
            'integer-projectlanguage_id' => ['projectlanguage_id', 'integer'],
        ];
    }

    /**
     * @param bool $skipIfSet
     *
     * @return $this
     * @throws \Throwable
     */
    public function fafcmsLoadDefaultValuesMultilingualTrait($skipIfSet = true): self
    {
        if (!$skipIfSet || $this->projectlanguage_id === null) {
            $this->projectlanguage_id = Yii::$app->fafcms->getCurrentProjectLanguageId();
        }

        return $this;
    }

    /**
     * Gets query for [[Projectlanguage]].
     *
     * @return ActiveQuery
     */
    public function getProjectlanguage(): ActiveQuery
    {
        return $this->hasOne(Projectlanguage::class, ['id' => 'projectlanguage_id']);
    }


    /**
     * @return ActiveQuery
     */
    public function getTranslations(): ActiveQuery
    {
        return $this->hasMany(static::class, [
            'translation_base_id' => 'id',
        ])->byProjectLanguage('all');
    }
}
