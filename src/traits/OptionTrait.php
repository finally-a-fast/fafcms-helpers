<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019-2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\traits;

use fafcms\helpers\classes\OptionProvider;
use Yii;

/**
 * Trait OptionTrait
 * @package fafcms\helpers\traits
 * @deprecated Please use OptionProviderTrait instead
 */
trait OptionTrait
{
    /**
     * @param bool $empty
     * @param array|null $select
     * @param array|null $sort
     * @param array|null $where
     * @param array|null $joinWith
     * @param string|null $emptyLabel
     * @return array
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        if ($empty) {
            return ['' => $emptyLabel??Yii::t('fafcms-core', '- None - ')];
        }

        return [];
    }
}
