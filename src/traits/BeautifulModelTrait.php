<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\traits;

use fafcms\helpers\ActiveRecord;
use fafcms\helpers\Model;

/**
 * Trait BeautifulModelTrait
 *
 * @package fafcms\helpers\traits
 */
trait BeautifulModelTrait
{
    use BeautifulModelUrlTrait;
    use BeautifulModelNameTrait;
    use BeautifulModelIconTrait;
    use BeautifulModelLabelTrait;

    /**
     * @param ActiveRecord|Model $model
     *
     * @return array
     */
    public function editData($model): array
    {
        return [
            'url' => $model->getEditDataUrl(),
            'icon' => $model->getEditDataIcon(),
            'plural' => $model->getEditDataPlural(),
            'singular' => $model->getEditDataSingular()
        ];
    }

    /**
     * @return array
     */
    public function getEditData(): array
    {
        return $this->editData($this);
    }
}
