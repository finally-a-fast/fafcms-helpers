<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\traits;

use fafcms\helpers\ActiveRecord;
use fafcms\helpers\Model;

/**
 * Trait BeautifulModelLabelTrait
 *
 * @package fafcms\helpers\traits
 */
trait BeautifulModelLabelTrait
{
    /**
     * @param ActiveRecord|Model $model
     * @param bool               $html
     * @param array              $params
     *
     * @return string
     */
    abstract public static function extendedLabel($model, bool $html = true, array $params = []): string;

    /**
     * @param bool  $html
     * @param mixed ...$params
     *
     * @return string
     */
    public function getExtendedLabel(bool $html = true, ...$params): string
    {
        return static::extendedLabel($this, $html, $params);
    }
}
