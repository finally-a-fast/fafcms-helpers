<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\helpers\traits;

use ArrayObject;
use fafcms\helpers\ActiveRecord;
use fafcms\helpers\Model;
use Yii;
use fafcms\fafcms\components\InjectorComponent;
use yii\base\InvalidArgumentException;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;
use yii\base\UnknownPropertyException;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;
use Closure;
use yii\validators\Validator;

/**
 * Trait ModelTrait
 *
 * @package fafcms\helpers\traits
 */
trait ModelTrait
{
    /**
     * @var string|null
     */
    protected ?string $editableScenario = null;

    /**
     * @var array related models indexed by the relation names
     */
    protected array $extensionRelatedModels = [];

    /**
     * @var array relation names indexed by their link attributes
     */
    protected array $extensionRelationsDependencies = [];

    /**
     * @var array attribute values indexed by attribute names
     */
    protected array $extensionAttributes = [];

    /**
     * @var array|null old attribute values indexed by attribute names.
     * This is `null` if the record [[isNewRecord|is new]].
     */
    protected ?array $oldExtensionAttributes = null;

    /**
     * @var array
     */
    protected array $extensionAttributeHandlers = [];

    public function init(): void
    {
        $this->extensionAttributeHandlers = $this->getModelExtension('Attributes');
    }

    /**
     * @return mixed
     */
    public function beforeValidate()
    {
        if ($this->editableScenario !== null && isset($this->scenarios()[$this->editableScenario])) {
            $this->setScenario($this->editableScenario);
        }

        return parent::beforeValidate();
    }

    /**
     * Returns static class instance, which can be used to obtain meta information.
     * @param bool $refresh whether to re-create static instance even, if it is already cached.
     *
     * @return object class instance.
     * @throws \yii\base\InvalidConfigException
     */
    public static function instance($refresh = false): object
    {
        return InjectorComponent::instance(static::class, $refresh);
    }

    /**
     * @param string $methodName
     * @param array  $data
     * @param bool|array|null   $result
     * @param string $resultAction
     */
    public function executeExtendedMethods(string $methodName, array $data, &$result = null, $resultAction = self::RESULT_ACTION_BOOL): void
    {
        $model = $this;
        $methodNames = preg_grep('/^'.$methodName.'/', get_class_methods($model));

        if (count($methodNames) > 0) {
            array_walk($methodNames, static function ($method) use ($model, $data, &$result, $resultAction, $methodName) {
                $methodResult = $model->$method(...$data);

                if ($resultAction === self::RESULT_ACTION_BOOL) {
                    if (!$methodResult) {
                        $result = false;
                    }
                } elseif ($resultAction === self::RESULT_ACTION_MERGE) {
                    $result = array_merge($result, $methodResult);
                }
            });
        }
    }

    /**
     * @param array $row
     *
     * @return ActiveRecord|Model|object
     * @throws \yii\base\InvalidConfigException
     */
    public static function instantiate($row)
    {
        return InjectorComponent::createObject(static::class);
    }

    //region search
    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        if (!$this->searchInternal($dataProvider, $params)) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * @param BaseDataProvider|null $dataProvider
     * @param array                   $params
     * @param null                    $query
     * @param string|null             $formName
     *
     * @return bool
     * @throws InvalidConfigException
     */
    protected function searchInternal(?BaseDataProvider &$dataProvider, array $params, $query = null, ?string $formName = null): bool
    {
        // TODO if it is a model and not active record use BaseDataProvider
        $dataProvider        ??= InjectorComponent::createObject(ActiveDataProvider::class);
        $dataProvider->query ??= $query ?? static::find();

        $this->load($params, $formName);

        return $this->validate();
    }

    /**
     * implements rules for search scenario
     *
     * @return array
     * @throws NotSupportedException
     */
    public function searchRules(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     *
     * @throws InvalidConfigException|NotSupportedException
     */
    public function getValidators()
    {
        if ($this->getScenario() === static::SCENARIO_SEARCH) {
            return $this->createSearchValidators();
        }

        return parent::getValidators();
    }

    /**
     * @return ArrayObject
     * @throws InvalidConfigException|NotSupportedException
     * @see createValidators()
     */
    public function createSearchValidators(): ArrayObject
    {
        $validators = new ArrayObject();

        foreach ($this->searchRules() as $rule) {
            if ($rule instanceof Validator) {
                $validators->append($rule);
            } elseif (is_array($rule) && isset($rule[0], $rule[1])) { // attributes, validator type
                $validator = Validator::createValidator($rule[1], $this, (array) $rule[0], array_slice($rule, 2));
                $validators->append($validator);
            } else {
                throw new InvalidConfigException('Invalid validation rule: a rule must specify both attribute names and validator type.');
            }
        }

        return $validators;
    }
    //endregion search

    /**
     * @param      $data
     * @param null $formName
     *
     * @return bool
     */
    public function load($data, $formName = null): bool
    {
        $result = parent::load($data, $formName);
        $this->executeExtendedMethods('fafcmsLoad', [$data, $formName], $result);

        return $result;
    }

    /**
     * @param null $attributeNames
     * @param bool $clearErrors
     *
     * @return bool
     */
    public function validate($attributeNames = null, $clearErrors = true): bool
    {
        $result = parent::validate($attributeNames, $clearErrors);
        $this->executeExtendedMethods('fafcmsValidate', [$attributeNames, $clearErrors], $result);

        return $result;
    }

    /**
     * @param bool $skipIfSet
     *
     * @return $this
     */
    public function loadDefaultValues($skipIfSet = true): self
    {
        if (method_exists(parent::class, 'loadDefaultValues')) {
            $result = parent::loadDefaultValues($skipIfSet);
        } else {
            $result = $this;
        }

        $this->executeExtendedMethods('fafcmsLoadDefaultValues', [$skipIfSet], $result);

        return $result;
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     *
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null): bool
    {
        if (method_exists(parent::class, 'save')) {
            $result = parent::save($runValidation, $attributeNames);
        } else {
            $result = true;
        }

        $this->executeExtendedMethods('fafcmsSave', [$runValidation, $attributeNames], $result);

        return $result;
    }

    /**
     * @param bool $showAllErrors
     *
     * @return array
     */
    public function getErrorSummary($showAllErrors): array
    {
        $result = parent::getErrorSummary($showAllErrors);
        $this->executeExtendedMethods('fafcmsGetErrorSummary', [$showAllErrors], $result, self::RESULT_ACTION_MERGE);

        return $result;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $result = parent::rules();
        $this->executeExtendedMethods('fafcmsRules', [], $result, self::RESULT_ACTION_MERGE);

        return $result;
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        $result = parent::attributeLabels();
        $this->executeExtendedMethods('fafcmsAttributeLabels', [], $result, self::RESULT_ACTION_MERGE);

        return $result;
    }

    /**
     * @return array
     */
    public function attributeHints(): array
    {
        $result = parent::attributeLabels();
        $this->executeExtendedMethods('fafcmsAttributeHints', [], $result, self::RESULT_ACTION_MERGE);

        return $result;
    }


    /**
     * @return array
     */
    public function extensionAttributes(): array
    {
        return array_keys($this->extensionAttributeHandlers);
    }

    /**
     * @param bool $extensionAttributes
     *
     * @return array
     */
    public function attributes(bool $extensionAttributes = false): array
    {
        if ($extensionAttributes) {
            return array_merge(parent::attributes(), $this->extensionAttributes());
        }

        return parent::attributes();
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasExtensionAttribute(string $name): bool
    {
        return isset($this->extensionAttributes[$name]) || in_array($name, $this->extensionAttributes(), true);
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function hasAttribute($name): bool
    {
        if ($this->hasExtensionAttribute($name)) {
            return true;
        }

        if (method_exists(parent::class, 'hasAttribute')) {
            return parent::hasAttribute($name);
        }

        return false;
    }

    /**
     * @param string $name
     *
     * @return mixed|null
     */
    public function getExtensionAttribute(string $name)
    {
        return $this->extensionAttributes[$name] ?? null;
    }

    /**
     * @param $name
     *
     * @return mixed|null
     */
    public function getAttribute($name)
    {
        $attribute = $this->getExtensionAttribute($name);

        if ($attribute !== null) {
            return $attribute;
        }

        if (method_exists(parent::class, 'getAttribute')) {
            return parent::getAttribute($name);
        }

        return null;
    }

    public function setExtensionAttribute(string $name, $value): void
    {
        if ($this->hasExtensionAttribute($name)) {
            if (
                !empty($this->extensionRelationsDependencies[$name])
                && (!array_key_exists($name, $this->extensionAttributes) || $this->extensionAttributes[$name] !== $value)
            ) {
                $this->resetExtensionDependentRelations($name);
            }

            $this->extensionAttributes[$name] = $value;
            self::callAttributeHandler($this, 'set', $name, [$value]);
        } else {
            throw new InvalidArgumentException(static::class . ' has no extension attribute named "' . $name . '".');
        }
    }

    /**
     * Sets the named attribute value.
     * @param string $name the attribute name
     * @param mixed $value the attribute value.
     * @throws InvalidArgumentException if the named attribute does not exist.
     * @see hasAttribute()
     */
    public function setAttribute($name, $value): void
    {
        if ($this->hasExtensionAttribute($name)) {
            $this->setExtensionAttribute($name, $value);
        } elseif (method_exists(parent::class, 'setAttribute')) {
            parent::setAttribute($name, $value);
        }
    }

    /**
     * @param $record
     * @param $row
     */
    public static function populateRecord($record, $row): void
    {
        parent::populateRecord($record, $row);

        foreach ($record->extensionAttributeHandlers as $attributeName => $attributeHandler) {
            $record->extensionAttributes[$attributeName] = self::callAttributeHandler($record, 'get', $attributeName);
        }

        $record->oldExtensionAttributes = $record->extensionAttributes;
        $record->extensionRelatedModels = [];
        $record->extensionRelationsDependencies = [];
    }

    /**
     * @param ModelTrait $record
     * @param string     $action
     * @param string     $name
     * @param array      $params
     *
     * @return mixed|null
     */
    protected static function callAttributeHandler(self $record, string $action, string $name, array $params = [])
    {
        $attributeHandler = $record->extensionAttributeHandlers[$name];

        if (is_array($attributeHandler) && isset($attributeHandler[$action])) {
            $attributeHandler = $attributeHandler[$action];
        } else {
            array_unshift($params, $action);
        }

        if (!($attributeHandler instanceof Closure)) {
            return null;
        }

        return $attributeHandler->call($record, ...$params);
    }

    protected function refreshInternal($record): bool
    {
        if ($record === null) {
            return false;
        }

        foreach (array_keys($record->extensionAttributeHandlers) as $attributeName) {
            $this->extensionAttributes[$attributeName] = self::callAttributeHandler($this, 'get', $attributeName);
        }

        $this->oldExtensionAttributes = $this->extensionAttributes;
        $this->extensionRelatedModels = [];
        $this->extensionRelationsDependencies = [];

        if (method_exists(parent::class, 'refreshInternal')) {
            return parent::refreshInternal($record);
        }

        return true;
    }

    /**
     * Returns all populated related records.
     * @return array an array of related records indexed by relation names.
     * @see getRelation()
     */
    public function getExtensionRelatedRecords(): array
    {
        return $this->extensionRelatedModels;
    }

    /**
     * Returns all populated related records.
     * @return array an array of related records indexed by relation names.
     * @see getRelation()
     */
    public function getRelatedRecords(): array
    {
        if (method_exists(parent::class, 'getRelatedRecords')) {
            return array_merge(parent::getRelatedRecords(), $this->getExtensionRelatedRecords());
        }

        return $this->getExtensionRelatedRecords();
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function isExtensionRelationPopulated(string $name): bool
    {
        return array_key_exists($name, $this->extensionRelatedModels);
    }

    /**
     * Populates the named relation with the related records.
     * Note that this method does not check if the relation exists or not.
     * @param string $name the relation name, e.g. `orders` for a relation defined via `getOrders()` method (case-sensitive).
     * @param ActiveRecordInterface|array|null $records the related records to be populated into the relation.
     * @see getRelation()
     */
    public function populateExtensionRelation(string $name, $records): void
    {
        foreach ($this->extensionRelationsDependencies as &$relationNames) {
            unset($relationNames[$name]);
        }

        $this->extensionRelatedModels[$name] = $records;
    }

    /**
     * Resets dependent related models checking if their links contain specific attribute.
     * @param string $attribute The changed attribute name.
     */
    protected function resetExtensionDependentRelations(string $attribute): void
    {
        foreach ($this->extensionRelationsDependencies[$attribute] as $relation) {
            unset($this->extensionRelatedModels[$relation]);
        }

        unset($this->extensionRelationsDependencies[$attribute]);
    }

    /**
     * Sets relation dependencies for a property
     *
     * @param string               $name            property name
     * @param ActiveQueryInterface $relation        relation instance
     * @param string|null          $viaRelationName intermediate relation
     */
    protected function setExtensionRelationDependencies(string $name, ActiveQueryInterface $relation, ?string $viaRelationName = null): void
    {
        if (empty($relation->via) && $relation->link) {
            foreach ($relation->link as $attribute) {
                $this->extensionRelationsDependencies[$attribute][$name] = $name;
                if ($viaRelationName !== null) {
                    $this->extensionRelationsDependencies[$attribute][] = $viaRelationName;
                }
            }
        } elseif ($relation->via instanceof ActiveQueryInterface) {
            $this->setExtensionRelationDependencies($name, $relation->via);
        } elseif (is_array($relation->via)) {
            [$viaRelationName, $viaQuery] = $relation->via;
            $this->setExtensionRelationDependencies($name, $viaQuery, $viaRelationName);
        }
    }

    /**
     * @var array[]
     */
    protected array $modelExtension = [];

    /**
     * @param string $type
     *
     * @return array
     */
    protected function &getModelExtension(string $type): array
    {
        if (($this->modelExtension[$type] ?? null) === null) {
            $this->modelExtension[$type] = [];
            $className = static::class;

            do {
                $this->modelExtension[$type][] = Yii::$app->injector->{'getModelExtension' . $type}($className);
            } while (($className = get_parent_class($className)) !== false);

            $this->modelExtension[$type] = array_merge(...$this->modelExtension[$type]);
        }

        return $this->modelExtension[$type];
    }

    /**
     * @param $name
     * @param $params
     *
     * @return mixed
     */
    public function __call($name, $params)
    {
        $methods = $this->getModelExtension('Methods');
        $lowerName = mb_strtolower($name);

        if (isset($methods[$lowerName]) && $methods[$lowerName] instanceof Closure) {
            return $methods[$lowerName]->call($this, ...$params);
        }

        $relations = $this->getModelExtension('Relations');

        if (isset($relations[$lowerName]) && $relations[$lowerName] instanceof Closure) {
            return $relations[$lowerName]->call($this, ...$params);
        }

        return parent::__call($name, $params);
    }

    /**
     * @param $name
     *
     * @return mixed
     * @throws UnknownPropertyException
     */
    public function __get($name)
    {
        $extensionAttributes = $this->extensionAttributes;

        if (isset($extensionAttributes[$name]) || array_key_exists($name, $extensionAttributes)) {
            return $extensionAttributes[$name];
        }

        if ($this->hasExtensionAttribute($name)) {
            return null;
        }

        if (isset($this->extensionRelatedModels[$name]) || array_key_exists($name, $this->extensionRelatedModels)) {
            return $this->extensionRelatedModels[$name];
        }

        $relation = $this->getExtensionRelation($name, false);

        if ($relation !== null && ($relation instanceof ActiveQueryInterface)) {
            $this->setExtensionRelationDependencies($name, $relation);
            return $this->extensionRelatedModels[$name] = $relation->findFor($name, $this);
        }

        return parent::__get($name);
    }

    /**
     * PHP setter magic method.
     * This method is overridden so that AR attributes can be accessed like properties.
     * @param string $name property name
     * @param mixed $value property value
     */
    public function __set($name, $value)
    {
        if ($this->hasExtensionAttribute($name)) {
            $extensionAttributes = &$this->extensionAttributes;

            if (
                !empty($this->extensionRelationsDependencies[$name])
                && (!array_key_exists($name, $extensionAttributes) || $extensionAttributes[$name] !== $value)
            ) {
                $this->resetExtensionDependentRelations($name);
            }

            $extensionAttributes[$name] = $value;
            self::callAttributeHandler($this, 'set', $name, [$value]);
        } /* elseif ($this->hasAttribute($name)) {
            if (
                !empty($this->extensionRelationsDependencies[$name])
                && (!array_key_exists($name, $this->_attributes) || $this->_attributes[$name] !== $value)
            ) {
                $this->resetExtensionDependentRelations($name);
            }
            //$this->$name = $value;
        }*/ else {
            try {
                parent::__set($name, $value);
            } catch (InvalidCallException $e) {
               // $this->$name = $value;
            }
        }
    }

    /**
     * Checks if a property value is null.
     * This method overrides the parent implementation by checking if the named attribute is `null` or not.
     * @param string $name the property name or the event name
     * @return bool whether the property value is null
     */
    public function __isset($name)
    {
        try {
            return $this->__get($name) !== null;
        } catch (\Throwable $t) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Sets a component property to be null.
     * This method overrides the parent implementation by clearing
     * the specified attribute value.
     * @param string $name the property name or the event name
     */
    public function __unset($name)
    {
        if ($this->hasExtensionAttribute($name)) {
            $extensionAttributes = &$this->extensionAttributes;

            unset($extensionAttributes[$name]);
            if (!empty($this->extensionRelationsDependencies[$name])) {
                $this->resetExtensionDependentRelations($name);
            }
        } elseif (array_key_exists($name, $this->extensionRelatedModels)) {
            unset($this->extensionRelatedModels[$name]);
        } elseif ($this->getExtensionRelation($name, false) === null) {
            parent::__unset($name);
        }
    }

    /**
     * Returns the relation object with the specified name.
     * A relation is defined by a getter method which returns an [[ActiveQueryInterface]] object.
     * It can be declared in either the Active Record class itself or one of its behaviors.
     *
     * @param string $name           the relation name, e.g. `orders` for a relation defined via `getOrders()` method (case-sensitive).
     * @param bool   $throwException whether to throw exception if the relation does not exist.
     *
     * @return ActiveQueryInterface|null the relational query object. If the relation does not exist
     * and `$throwException` is `false`, `null` will be returned.
     * @throws InvalidArgumentException if the named relation does not exist.
     */
    public function getExtensionRelation(string $name, bool $throwException = true): ?ActiveQueryInterface
    {
        $getter = 'get' . mb_strtolower($name);
        $relations = $this->getModelExtension('Relations');

        if (isset($relations[$getter]) && $relations[$getter] instanceof Closure) {
            $relation = $relations[$getter]->call($this);
        } else {
            if ($throwException) {
                throw new InvalidArgumentException(static::class . ' has no relation named "' . $name . '".');
            }

            return null;
        }

        if (!($relation instanceof ActiveQueryInterface)) {
            if ($throwException) {
                throw new InvalidArgumentException(static::class . ' has no relation named "' . $name . '".');
            }

            return null;
        }

        return $relation;
    }

    /**
     * {@inheritdoc}
     */
    public function getRelation($name, $throwException = true)
    {
        $relation = $this->getExtensionRelation($name, false);

        if ($relation !== null) {
            return $relation;
        }

        if (method_exists(parent::class, 'getRelation')) {
            return parent::getRelation($name, $throwException);
        }

        return null;
    }

    /**
     * @param array $values
     */
    public function setOldExtensionAttributes(array $values): void
    {
        $this->oldExtensionAttributes = $values;
    }

    /**
     * {@inheritdoc}
     */
    public function setOldAttributes($values): void
    {
        //todo loop and check if attribute is an ExtensionAttribute
        parent::setOldAttributes($values);
    }

    /**
     * Returns the old attribute values.
     * @return array the old attribute values (name-value pairs)
     */
    public function getOldExtensionAttributes(): array
    {
        return $this->oldExtensionAttributes ?? [];
    }

    /**
     * {@inheritdoc}
     */
    public function getOldAttributes(): array
    {
        if (method_exists(parent::class, 'getOldAttributes')) {
            return array_merge(parent::getOldAttributes(), $this->getOldExtensionAttributes());
        }

        return $this->getOldExtensionAttributes();
    }

    /**
     * @param string $name
     *
     * @return mixed|null
     */
    public function getOldExtensionAttribute(string $name)
    {
        return $this->oldExtensionAttributes[$name] ?? null;
    }

    /**
     * {@inheritdoc}
     */
    public function getOldAttribute($name)
    {
        $value = $this->getOldExtensionAttribute($name);

        if ($value === null && method_exists(parent::class, 'getOldAttribute')) {
            return parent::getOldAttribute($name);
        }

        return $value;
    }

    /**
     * Sets the old value of the named attribute.
     * @param string $name the attribute name
     * @param mixed $value the old attribute value.
     * @throws InvalidArgumentException if the named attribute does not exist.
     * @see hasExtensionAttribute()
     */
    public function setOldExtensionAttribute(string $name, $value): void
    {
        if (isset($this->oldExtensionAttributes[$name]) || $this->hasExtensionAttribute($name)) {
            $this->oldExtensionAttributes[$name] = $value;
        } else {
            throw new InvalidArgumentException(static::class . ' has no extension attribute named "' . $name . '".');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setOldAttribute($name, $value): void
    {
        if (isset($this->oldExtensionAttributes[$name]) || $this->hasExtensionAttribute($name)) {
            $this->setOldExtensionAttribute($name, $value);
        } elseif (method_exists(parent::class, 'setOldAttribute')) {
            parent::setOldAttribute($name, $value);
        } else {
            throw new InvalidArgumentException(static::class . ' has no extension attribute named "' . $name . '".');
        }
    }

    /**
     * Marks an attribute dirty.
     * This method may be called to force updating a record when calling [[update()]],
     * even if there is no change being made to the record.
     *
     * @param string $name the attribute name
     */
    public function markExtensionAttributeDirty(string $name): void
    {
        unset($this->oldExtensionAttributes[$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function markAttributeDirty($name): void
    {
        $this->markExtensionAttributeDirty($name);

        if (method_exists(parent::class, 'markAttributeDirty')) {
            parent::markAttributeDirty($name);
        }
    }

    /**
     * Returns a value indicating whether the named attribute has been changed.
     *
     * @param string $name      the name of the attribute.
     * @param bool   $identical whether the comparison of new and old value is made for
     *                          identical values using `===`, defaults to `true`. Otherwise `==` is used for comparison.
     *                          This parameter is available since version 2.0.4.
     *
     * @return bool whether the attribute has been changed
     */
    public function isExtensionAttributeChanged(string $name, bool $identical = true): bool
    {
        $attributes = $this->extensionAttributes;

        if (isset($attributes[$name], $this->oldExtensionAttributes[$name])) {
            if ($identical) {
                return $attributes[$name] !== $this->oldExtensionAttributes[$name];
            }

            /** @noinspection TypeUnsafeComparisonInspection */
            return $attributes[$name] != $this->oldExtensionAttributes[$name];
        }

        return isset($attributes[$name]) || isset($this->oldExtensionAttributes[$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function isAttributeChanged($name, $identical = true): bool
    {
        if (isset($this->oldExtensionAttributes[$name])|| isset($this->extensionAttributes[$name])) {
            return $this->isExtensionAttributeChanged($name, $identical);
        }

        if (method_exists(parent::class, 'isAttributeChanged')) {
            return parent::isAttributeChanged($name);
        }

        return false;
    }

    /**
     * Returns the attribute values that have been modified since they are loaded or saved most recently.
     *
     * The comparison of new and old values is made for identical values using `===`.
     *
     * @param string[]|null $names the names of the attributes whose values may be returned if they are
     * changed recently. If null, [[attributes()]] will be used.
     * @return array the changed attribute values (name-value pairs)
     */
    public function getDirtyExtensionAttributes($names = null): ?array
    {
        if ($names === null) {
            $names = $this->extensionAttributes();
        }

        $names = array_flip($names);
        $attributes = [];

        if ($this->oldExtensionAttributes === null) {
            foreach ($this->extensionAttributes as $name => $value) {
                if (isset($names[$name])) {
                    $attributes[$name] = $value;
                }
            }
        } else {
            foreach ($this->extensionAttributes as $name => $value) {
                if (isset($names[$name]) && (!array_key_exists($name, $this->oldExtensionAttributes) || $value !== $this->oldExtensionAttributes[$name])) {
                    $attributes[$name] = $value;
                }
            }
        }

        return $attributes;
    }

    /**
     * {@inheritdoc}
     */
    public function getDirtyAttributes($names = null, bool $extensionAttributes = false): ?array
    {
        $dirtyAttributes = [];

        if (method_exists(parent::class, 'getDirtyAttributes')) {
            $dirtyAttributes = parent::getDirtyAttributes($names);
        }

        if ($extensionAttributes) {
            return array_merge($dirtyAttributes, $this->getDirtyExtensionAttributes($names));
        }

        return $dirtyAttributes;
    }

    /**
     * Updates the specified attributes.
     *
     * This method is a shortcut to [[update()]] when data validation is not needed
     * and only a small set attributes need to be updated.
     *
     * You may specify the attributes to be updated as name list or name-value pairs.
     * If the latter, the corresponding attribute values will be modified accordingly.
     * The method will then save the specified attributes into database.
     *
     * Note that this method will **not** perform data validation and will **not** trigger events.
     *
     * @param array $attributes the attributes (names or name-value pairs) to be updated
     *
     * @return int the number of rows affected.
     */
    public function updateExtensionAttributes(array $attributes): int
    {
        $attrs = [];

        foreach ($attributes as $name => $value) {
            if (is_int($name)) {
                $attrs[] = $value;
            } else {
                $this->$name = $value;
                $attrs[] = $name;
            }
        }

        $values = $this->getDirtyExtensionAttributes($attrs);

        if (empty($values) || $this->getIsNewRecord()) {
            return 0;
        }

        foreach ($values as $name => $value) {
            self::callAttributeHandler($this, 'before-update', $name, [$value]);
            self::callAttributeHandler($this, 'update', $name, [$value]);
            $this->oldExtensionAttributes[$name] = $value;
            self::callAttributeHandler($this, 'after-update', $name, [$value]);
        }

        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function updateAttributes($attributes): int
    {
        if (method_exists(parent::class, 'updateAttributes')) {
            $allExtensionAttributes = $this->extensionAttributes;

            $extensionAttributes = array_filter($attributes, static function($v, $k) use ($allExtensionAttributes) {
                if (is_int($k)) {
                    return isset($allExtensionAttributes[$v]);
                }

                return isset($allExtensionAttributes[$k]);
            }, ARRAY_FILTER_USE_BOTH);

            $normalAttributes = array_filter($attributes, static function($v, $k) use ($allExtensionAttributes) {
                if (is_int($k)) {
                    return !isset($allExtensionAttributes[$v]);
                }

                return !isset($allExtensionAttributes[$k]);
            }, ARRAY_FILTER_USE_BOTH);

            $rows = $this->updateExtensionAttributes($extensionAttributes) + parent::updateAttributes($normalAttributes);

            return $rows === 0 ? 0 : 1;
        }

        return $this->updateExtensionAttributes($attributes);
    }

    /**
     * @param null $attributes
     */
    protected function updateExtensionInternal($attributes = null): void
    {
        $values = $this->getDirtyExtensionAttributes($attributes);

        foreach ($values as $name => $value) {
            self::callAttributeHandler($this, 'before-update', $name, [$value]);
            self::callAttributeHandler($this, 'update', $name, [$value]);
            $this->oldExtensionAttributes[$name] = $value;
            self::callAttributeHandler($this, 'after-update', $name, [$value]);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function updateInternal($attributes = null)
    {
        $result = 0;

        if (method_exists(parent::class, 'updateInternal')) {
            $result = parent::updateInternal($attributes);
        }

        if ($result !== false) {
            $this->updateExtensionInternal($attributes);
        }

        return $result;
    }

    /**
     * Updates one or several counter columns for the current AR object.
     * Note that this method differs from [[updateAllCounters()]] in that it only
     * saves counters for the current AR object.
     *
     * An example usage is as follows:
     *
     * ```php
     * $post = Post::findOne($id);
     * $post->updateCounters(['view_count' => 1]);
     * ```
     *
     * @param array $counters the counters to be updated (attribute name => increment value)
     * Use negative values if you want to decrement the counters.
     * @return bool whether the saving is successful
     * @see updateAllCounters()
     */
    public function updateExtensionCounters($counters)
    {
        if (count($counters) > 0) {
            foreach ($counters as $name => $value) {
                if (!isset($this->extensionAttributes[$name])) {
                    $this->extensionAttributes[$name] = $value;
                } else {
                    $this->extensionAttributes[$name] += $value;
                }

                self::callAttributeHandler($this, 'before-update', $name, [$this->extensionAttributes[$name]]);
                self::callAttributeHandler($this, 'update', $name, [$this->extensionAttributes[$name]]);
                $this->oldExtensionAttributes[$name] = $this->extensionAttributes[$name];
                self::callAttributeHandler($this, 'after-update', $name, [$this->extensionAttributes[$name]]);
            }

            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function updateCounters($counters): bool
    {
        $allExtensionAttributes = $this->extensionAttributes;

        $extensionAttributes = array_filter($counters, static function($k) use ($allExtensionAttributes) {
            return isset($allExtensionAttributes[$k]);
        }, ARRAY_FILTER_USE_KEY);

        $normalAttributes = array_filter($counters, static function($k) use ($allExtensionAttributes) {
            return !isset($allExtensionAttributes[$k]);
        }, ARRAY_FILTER_USE_KEY);

        $result = false;

        if (count($normalAttributes) > 0) {
            $result = parent::updateCounters($normalAttributes);
        }

        return $this->updateExtensionCounters($extensionAttributes) || $result;
    }

    /**
     * @param array $link
     * @param ActiveRecordInterface $foreignModel
     * @param ActiveRecordInterface $primaryModel
     * @throws InvalidCallException
     */
    protected function bindExtensionModels(array $link, ActiveRecordInterface $foreignModel, ActiveRecordInterface $primaryModel): void
    {
        foreach ($link as $fk => $pk) {
            $value = $primaryModel->$pk;
            if ($value === null) {
                throw new InvalidCallException('Unable to link models: the primary key of ' . get_class($primaryModel) . ' is null.');
            }
            if (is_array($foreignModel->$fk)) { // relation via array valued attribute
                $foreignModel->{$fk}[] = $value;
            } else {
                $foreignModel->{$fk} = $value;
            }
        }

        $foreignModel->save(false);
    }

    /**
     * Establishes the relationship between two models.
     *
     * The relationship is established by setting the foreign key value(s) in one model
     * to be the corresponding primary key value(s) in the other model.
     * The model with the foreign key will be saved into database without performing validation.
     *
     * If the relationship involves a junction table, a new row will be inserted into the
     * junction table which contains the primary key values from both models.
     *
     * Note that this method requires that the primary key value is not null.
     *
     * @param string                $name         the case sensitive name of the relationship, e.g. `orders` for a relation defined via `getOrders()` method.
     * @param ActiveRecordInterface $model        the model to be linked with the current one.
     * @param array|null            $extraColumns additional column values to be saved into the junction table.
     *                                            This parameter is only meaningful for a relationship involving a junction table
     *                                            (i.e., a relation set with [[ActiveRelationTrait::via()]] or [[ActiveQuery::viaTable()]].)
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function linkExtension(string $name, ActiveRecordInterface $model, ?array $extraColumns = []): void
    {
        $relation = $this->getExtensionRelation($name);

        if ($relation->via !== null) {
            if ($this->getIsNewRecord() || $model->getIsNewRecord()) {
                throw new InvalidCallException('Unable to link models: the models being linked cannot be newly created.');
            }

            if (is_array($relation->via)) {
                /* @var $viaRelation ActiveQuery */
                [$viaName, $viaRelation] = $relation->via;
                $viaClass = $viaRelation->modelClass;
                // unset $viaName so that it can be reloaded to reflect the change
                unset($this->extensionRelatedModels[$viaName]);
            } else {
                $viaRelation = $relation->via;
                $viaTable = reset($relation->via->from);
            }

            $columns = [];

            foreach ($viaRelation->link as $a => $b) {
                $columns[$a] = $this->$b;
            }

            foreach ($relation->link as $a => $b) {
                $columns[$b] = $model->$a;
            }

            foreach ($extraColumns as $k => $v) {
                $columns[$k] = $v;
            }

            if (is_array($relation->via)) {
                /* @var $viaClass ActiveRecordInterface */
                /* @var $record ActiveRecordInterface */
                $record = Yii::createObject($viaClass);

                foreach ($columns as $column => $value) {
                    $record->$column = $value;
                }

                $record->insert(false);
            } else {
                /* @var $viaTable string */
                static::getDb()->createCommand()
                    ->insert($viaTable, $columns)->execute();
            }
        } else {
            $p1 = $model->isPrimaryKey(array_keys($relation->link));
            $p2 = static::isPrimaryKey(array_values($relation->link));

            if ($p1 && $p2) {
                if ($this->getIsNewRecord() && $model->getIsNewRecord()) {
                    throw new InvalidCallException('Unable to link models: at most one model can be newly created.');
                } elseif ($this->getIsNewRecord()) {
                    $this->bindExtensionModels(array_flip($relation->link), $this, $model);
                } else {
                    $this->bindExtensionModels($relation->link, $model, $this);
                }
            } elseif ($p1) {
                $this->bindExtensionModels(array_flip($relation->link), $this, $model);
            } elseif ($p2) {
                $this->bindExtensionModels($relation->link, $model, $this);
            } else {
                throw new InvalidCallException('Unable to link models: the link defining the relation does not involve any primary key.');
            }
        }

        // update lazily loaded related objects
        if (!$relation->multiple) {
            $this->extensionRelatedModels[$name] = $model;
        } elseif (isset($this->extensionRelatedModels[$name])) {
            if ($relation->indexBy !== null) {
                if ($relation->indexBy instanceof \Closure) {
                    $index = call_user_func($relation->indexBy, $model);
                } else {
                    $index = $model->{$relation->indexBy};
                }
                $this->extensionRelatedModels[$name][$index] = $model;
            } else {
                $this->extensionRelatedModels[$name][] = $model;
            }
        }
    }

    /**
     * {@inheritdoc}
     * @throws \yii\base\InvalidConfigException
     */
    public function link($name, $model, $extraColumns = []): void
    {
        $relation = $this->getExtensionRelation($name, false);

        if ($relation !== null) {
            $this->linkExtension($name, $model, $extraColumns);
        } elseif (method_exists(parent::class, 'link')) {
            parent::link($name, $model, $extraColumns);
        }
    }
}
