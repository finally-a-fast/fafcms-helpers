<?php

namespace fafcms\helpers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Class AccessControl
 * @package app\helpers
 */
class AccessControl extends \yii\filters\AccessControl
{
    /**
     * @param false|\yii\web\User $user
     *
     * @throws ForbiddenHttpException
     * @throws \JsonException
     */
    protected function denyAccess($user): void
    {
        if ($user !== false && $user->getIsGuest()) {
            if (Yii::$app->getRequest()->getIsAjax()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->user->setReturnUrl(Yii::$app->request->getReferrer());

                throw new ForbiddenHttpException(json_encode(ArrayHelper::merge([
                    'url' => Url::to(Yii::$app->user->loginUrl)
                ], Yii::$app->fafcms->loginAjaxResponse), JSON_THROW_ON_ERROR), 42);
            }

            $user->loginRequired();
        } else {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }
}
