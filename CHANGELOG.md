[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Helpers
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Function to export single models @cmoeke
- Added AttributeOptionTrait to normalize attributesOptions functions @cmoeke
- Moved model query map to InjectorComponent::getActiveQueryForModel @cmoeke
- Class constants for basic scenarios in ActiveRecord.php#28 @StefanBrandenburger
- scenarios function in ActiveRecord.php#145 @StefanBrandenburger
- Model trait and moved scenario into it @cmoeke
- Base model class @cmoeke
- Private property _scenarios in ActiveRecord.php#64 @StefanBrandenburger
- Functions search, searchInternal, searchRules and createSearchValidators to implement search model functionality in every ActiveRecord class @StefanBrandenburger
- Possibility to inject relations and attributes to models and active records @cmoeke
- Possibility to register es6 modules @cmoeke
- Possibility to check for needed js polyfills @cmoeke
- Missing key to js with options array to prevent duplication of assets @cmoeke
- Option to register js files and modules without minification @cmoeke
- Polyfill for IntersectionObserver @cmoeke
- Polyfill for Symbol @cmoeke
- Possibility to add polyfills as js module @cmoeke
- Registered asset as return in registerModule function @cmoeke
- New cleaner OptionProvider and OptionProviderTrait @cmoeke
- Multi language migration and trait @cmoeke
- Multi project migration and trait @cmoeke
- Functions to translate items @cmoeke
- Option to export all items @cmoeke
- Fafcms asset component bundle @cmoeke

### Changed
- Function getAttributeOptions of AttributeOptionTrait.php#30 to accept variable number of arguments wich are passed to closure call @StefanBrandenburger 
- Replaced Yii::createObject with InjectorComponent::createObject @cmoeke
- Scenarios now get stored in _scenarios variable in ActiveRecord.php @StefanBrandenburger
- Overwrite getValidators function of ActiveRecord.php to use createSearchValidators on search scenario @StefanBrandenburger
- Moved ContentmetaTrait to sitemanager module @cmoeke
- Improved performance of TagTrait.php @cmoeke
- Changed polyfill for URLSearchParams @cmoeke
- Changed db side timestamp to php DateTime format @cmoeke
- GetAttributeOptions to make it possible to get options for a attribute without calling the closure @cmoeke
- Moved "renderActionContent" to new render component @cmoeke
- Yii view placeholder to make it fafte compatible @cmoeke
- Updated composer.json @cmoeke
- Renamed class finder cache @cmoeke

### Fixed
- Bug when file cannot be compressed @cmoeke
- Text export @cmoeke
- Fixed disabled or readonly widget inputs @cmoeke
- Compatibility of search function in ActiveRecord.php#405 @StefanBrandenburger
- Wrong namespaces @cmoeke
- Bug when accessing model data inside of an extension attribute of a model @cmoeke
- Fixes in ClassFinder.php for php8 @cmoeke
- Fixed $targetPath in asset converter @cmoeke

### Removed
- Arrow functions to improve IE11 support @cmoeke
- Removed materialize @cmoeke

## [0.1.0] - 2020-09-29
### Added
- CHANGELOG.md @cmoeke fafcms-core#39
- Basic doc folder structure @cmoeke fafcms-core/37
- .gitlab-ci.yml for doc an build creation @cmoeke fafcms-core/38
- $authModelClass to make it possible to check access rules of a real model instead of a used form model @cmoeke
- Rendering of title and breadcrumb in ajax requests @cmoeke
- Added loginAjaxResponse to login ajax response to make it customizable @cmoeke
- Added auto minimization and combination of js and css files @cmoeke
- Added option to disable log for model changes @cmoeke

### Changed
- composer.json to use correct dependencies @cmoeke
- LICENSE to LICENSE.md @cmoeke
- Updated async css options for new loadCSS version @cmoeke
- PHP dependency to 7.4 @cmoeke
- Changed ci config to handle new build branch @cmoeke
- Changed minifier to fafcms minifier and enabled it @cmoeke
- Naming of AttributeOptionTrait.php functions to ensure compatibility with older versions @StefanBrandenburger

### Fixed
- Broken README.md icons @cmoeke fafcms-core#46
- The AttributesBehaviors check method returns now false if the app is not installed and if model is currently generating by gii @StefanBrandenburger

### Removed
- Removed momentjs date format converter @cmoeke

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-helpers/-/tree/master
[0.1.0]: https://gitlab.com/finally-a-fast/fafcms-helpers/-/tree/v0.1.0-alpha
